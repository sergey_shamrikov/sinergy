﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Producer</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
    <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="container-fluid">
       <div class="row">
        <div class="col-lg-2 p_rel">
          <div class="catalog_box catalog_hidden">
            <h5 class="catalog_title">Каталог</h5>
            <ul class="catalog_list">
              <li class="catalog_item">
                <a href="#" class="catalog_link">Интегральные микросхемы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Аудио</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Предохранители, фильтры</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Фильтры</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Карты и модули памяти</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Реле</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Радиочастотные компоненты</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Трансформаторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Конденсаторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Датчики, сенсоры</a>
                <ul class="sub_catalog_list">
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Аксессуары</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Модульные</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Пленочные</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Танталовые</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                  </li>
                </ul>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Дискретные полупроводники</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Разъемы, соединители</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <ul class="breadcrumbs clearfix">
              <li class="breadcrumbs_item d_in-block">
                <a href="sinergia_providers.html" class="breadcrumbs_link">
                  <span class="breadcrumbs_current">Поставщики</span>
                </a>
                /
              </li>
              <li class="breadcrumbs_item d_in-block">
                <span class="breadcrumbs_current">Fairchild</span>
              </li>
            </ul>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row mb48">
        <div class="col-lg-2">
          <div class="provider-logo2 al_right">
            <a class="provider_link" href="http://www.fairchild.com">
              <img src="images/3_m.png" alt="">
            </a>
          </div>
        </div>
        <div class="col-lg-6">
          <h3 class="title1 prov_titel_item">3M Products</h3>
          <p class="mb10">
            3M offers a broad range of interconnect solutions for the electronics industry. Our portfolio includes advanced rectangular I/O solutions<br> in our 3M™ Mini Delta Ribbon (MDR), 3M™ Shrunk Delta Ribbon (SDR) and 3M™ Shielded Compact Ribbon (SCR) product lines, as well as cable assemblies and assembly tooling to help you conquer today's high-end signaling challenges. For a global network of skilled engineering support and I/O products with excellent signal integrity, compact size and proven reliability… 3M™ Interconnects has the answer.
          </p>
          <div class="link_item">
            <a class="www_lk" href="http://www.fairchild.com">www.3m.com</a>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="video_box">
            <iframe height="198" src="//www.youtube.com/embed/qwFqRoDb08k" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
    <div class="bg_black mb2">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="title2 al_center pt35 pb32">21 427 компонентов в 30 категориях</h3>
          </div>
        </div>
        <div class="row mb38">
          <div class="col-lg-3">
            <ul class="components_list">
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Socket
                  </a>
                </p>
                <span class="p_abs">765</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Electronic Test Equipment
                  </a>
                </p>
                <span class="p_abs">318</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Bumpers and Leveling Elements
                  </a>
                </p>
                <span class="p_abs">196</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Memory Card
                  </a>
                </p>
                <span class="p_abs">169</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Cable Assembly
                  </a>
                </p>
                <span class="p_abs">138</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector D-Subminiature
                  </a>
                </p>
                <span class="p_abs">110</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    30  Parts
                  </a>
                </p>
                <span class="p_abs">61</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Cable Accessories
                  </a>
                </p>
                <span class="p_abs">56</span>
              </li>
            </ul>
          </div>
          <div class="col-lg-3">
            <ul class="components_list">
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Cable Accessories
                  </a>
                </p>
                <span class="p_abs">110</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Card Edge
                  </a>
                </p>
                <span class="p_abs">61</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Boxes, Enclosures and Racks
                  </a>
                </p>
                <span class="p_abs">56</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Epoxy Adhesives
                  </a>
                </p>
                <span class="p_abs">39</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Backplane
                  </a>
                </p>
                <span class="p_abs">38</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Component Kits
                  </a>
                </p>
                <span class="p_abs">36</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Fiber Optics
                  </a>
                </p>
                <span class="p_abs">35</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Hardware Tools
                  </a>
                </p>
                <span class="p_abs">156</span>
              </li>
            </ul>
          </div>
          <div class="col-lg-3">
            <ul class="components_list">
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Headers and PCB Receptacles
                  </a>
                </p>
                <span class="p_abs">138</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Misc Products
                  </a>
                </p>
                <span class="p_abs">11 038</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Cables
                  </a>
                </p>
                <span class="p_abs">36</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Accessories
                  </a>
                </p>
                <span class="p_abs">35</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Tapes
                  </a>
                </p>
                <span class="p_abs">169</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Terminals
                  </a>
                </p>
                <span class="p_abs">138</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector SCSI
                  </a>
                </p>
                <span class="p_abs">110</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Socket
                  </a>
                </p>
                <span class="p_abs">61</span>
              </li>
            </ul>
          </div>
          <div class="col-lg-3">
            <ul class="components_list">
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Backshells
                  </a>
                </p>
                <span class="p_abs">2 196</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Cable Markers
                  </a>
                </p>
                <span class="p_abs">56</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Power
                  </a>
                </p>
                <span class="p_abs">39</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Assembly Tools
                  </a>
                </p>
                <span class="p_abs">38</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Development Kits and Tools
                  </a>
                </p>
                <span class="p_abs">36</span>
              </li>
              <li class="list2_item p_rel">
                <p>
                  <a href="#">
                    Connector Rectangular
                  </a>
                </p>
                <span class="p_abs">35</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>