﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>News</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
 <!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
<div id="content">
  <div class="sh_box">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-2 p_rel">
          <div class="catalog_box catalog_hidden">
            <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
              </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <h2 class="title1 catalog_user">Новости</h2>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <ul class="list_news general">
            <li class="list_news_item clearfix">
              <div class="news_data f_left">
                <strong>18</strong>
                <span>12.14</span>
              </div> 
              <p class="wrapper">
                Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены...для подавления.
              </p>
            </li>
            <li class="list_news_item clearfix">
              <div class="news_data f_left">
                <strong>24</strong>
                <span>10.14</span>
              </div>
              <p class="wrapper">
                 Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы...
                  </p>
            </li>
            <li class="list_news_item clearfix">
              <div class="news_data f_left">
                <strong>16</strong>
                <span>09.14</span>
              </div>
              <p class="wrapper">
                Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как... выводы. 
              </p>
            </li>
            <li class="list_news_item clearfix">
              <div class="news_data f_left">
                <strong>02</strong>
                <span>09.14</span>
              </div>
              <p class="wrapper">
                Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены...для подавления.
              </p>
            </li>
            <li class="list_news_item clearfix">
              <div class="news_data f_left">
                <strong>17</strong>
                <span>08.14</span>
              </div>
              <p class="wrapper">
                Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы...
              </p>
            </li>
          </ul>
        </div>
        <div class="col-lg-6">
          <ul class="list_news general">
                <li class="list_news_item clearfix">
                  <div class="news_data f_left">
                    <strong>07</strong>
                    <span>12.14</span>
                  </div> 
                  <p class="wrapper">
                    Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются...выводы. 
                  </p>
                </li>
                <li class="list_news_item clearfix">
                  <div class="news_data f_left">
                    <strong>18</strong>
                    <span>10.14</span>
                  </div>
                  <p class="wrapper">
                    Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены... для подавления.
                  </p>
                </li>
                <li class="list_news_item clearfix">
                  <div class="news_data f_left">
                    <strong>04</strong>
                    <span>09.14</span>
                  </div>
                  <p class="wrapper">
                    Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы...
                  </p>
                </li>
                <li class="list_news_item clearfix">
                  <div class="news_data f_left">
                    <strong>27</strong>
                    <span>08.14</span>
                  </div>
                  <p class="wrapper">
                    Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются...как выводы. 
                  </p>
                </li>
                <li class="list_news_item clearfix">
                  <div class="news_data f_left">
                    <strong>06</strong>
                    <span>08.14</span>
                  </div>
                  <p class="wrapper">
                    Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки  вы сможете создать нереальную. Рассчитаны на высокий. Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления. Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются...как выводы. 
                  </p>
                </li>
          </ul>
        </div>
        <div class="col-lg-12 mt30">
          <div class="al_center ">
            <a href="javascript:;" class="white_bd_lk">Показать еще</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>