 <header>
    <div class="header_red_bg p_abs"></div>
      <div class="container-fluid">
        <div class="row">
          <div class="header_table">
            <div class="header_cel1">
              <div class="custom_select select_city">
               <select>
                <option value="1">Санкт-Петербург</option>
                <option value="2">Москва</option>
                <option value="3">Питер</option>
               </select>
              </div>
              <!-- <div class="header_select select">Санкт-Петербург</div> -->
              <div role="banner" class="logo">
                <a href="index.html"><img src="images/logo.png" alt="">HomeStyle</a>
              </div>
            </div>
            <div class="header_cel2">
              <div class="header_top clearfix">
                <div class="f_left">
                  <div class="that_order drop_down_box d_ib">
                    <span class="drop_down_btn black">Что с моим заказом?</span>
                    <div class="drop_down">
                      <form class="dropdown_form">
                        <input type="text" class="mb6" value="Номер заказа" onblur="if(this.value=='') this.value='Номер заказа'" onfocus="if(this.value =='Номер заказа' ) this.value=''">
                        <input type="text" class="mb16" value="Эл. почта" onblur="if(this.value=='') this.value='Эл. почта'" onfocus="if(this.value =='Эл. почта' ) this.value=''">
                        <div class="form_box d_tb mb16">
                          <div class="w165 d_tc">
                            <input type="text" value="Код проверки" onblur="if(this.value=='') this.value='Код проверки'" onfocus="if(this.value =='Код проверки' ) this.value=''">
                          </div>
                          <span class="form_box_code al_center d_tc">
                            <img src="images/form_code.png" alt="code">
                          </span>
                          <button type="button" class="update_btn d_tc"></button>
                        </div>
                        <div class="al_right">
                          <button class="red_btn">Узнать статус доставки</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <a href="#" class="header_top_link1 black">Как купить</a>
                  <a href="#" class="header_top_link1 black">Оплата и доставка</a>
                </div>
                <div class="tel_box f_right">
                  <span class="tel_number black">+7 812 923 25 29</span>
                  <div class="request_call drop_down_box d_ib">
                    <span class="drop_down_btn">Заказать звонок</span>
                    <div class="drop_down drop_down_right">
                      <form class="dropdown_form p_rel">
                        <input type="text" class="mb6" value="Ваше имя" onblur="if(this.value=='') this.value='Ваше имя'" onfocus="if(this.value =='Ваше имя' ) this.value=''">
                        <input type="text" class="phone mb6" value="Эл. почта" onblur="if(this.value=='') this.value='Эл. почта'" onfocus="if(this.value =='Эл. почта' ) this.value=''">
                        <textarea class="mb6" onblur="if(this.value=='') this.value='Комментарий'" onfocus="if(this.value =='Комментарий' ) this.value=''">Комментарий</textarea>
                        <div class="calendar_box pb20">
                        <span class="d_in-block val_mid">Когда позвонить:</span>
                        <input type="text" id="mydate" gldp-id="mydate" class="click_date d_ib">
                        <div gldp-el="mydate" class="calendar_select"></div>
                        </div>
                        <div class="al_right">
                          <button class="red_btn">Заказать звонок</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="search_box clearfix p_rel">
                <form class="">
                  <div class="search_select p_abs">
                    <div class="custom_select search_select_inner">
                      <select>
                        <option>параметры</option>
                        <option>продукт</option>
                        <option>новости</option>
                        <option>поставщики</option>
                      </select>
                    </div>
                  </div>
                  <input type="text" class="search_input d_block" value="Поиск по компонентам" onblur="if(this.value=='') this.value='Поиск по компонентам'" onfocus="if(this.value =='Поиск по компонентам' ) this.value=''">
                  <button class="seach_btn p_abs"><span></span>Найти</button>
                </form>
              </div>
            </div>
            <div class="header_cel3 al_right">
              <div class="log_in_box">
                <div class="log_in_link drop_down_box d_ib">
                  <span class="drop_down_btn black">Вход</span>
                  <div class="drop_down drop_down_right">
                      <form class="dropdown_form">
                        <input type="text" class="mb6" value="Эл.адрес" onblur="if(this.value=='') this.value='Эл.адрес'" onfocus="if(this.value =='Эл.адрес' ) this.value=''">
                        <input type="text" class="mb20" value="Пароль" onblur="if(this.value=='') this.value='Пароль'" onfocus="if(this.value =='Пароль' ) this.value=''">
                        <div class="log_inner al_just">
                        	<button class="white_bd_lk">Забыли пароль?</button>
                          <button class="red_btn ml10">Вход</button>
                        </div>
                      </form>
                    </div>
                </div>
                <span class="black">|</span>
                <div class="registration_link drop_down_box d_ib">
                  <span class="drop_down_btn black">регистрация</span>
                  <div class="drop_down">
                    
                  </div>
                </div>
              </div>
              <div class="basket_box">
                <div class="drop_down_box">
                  <a href="#" class="red_btn drop_down_btn">
                    <span class="basket_icon"></span>
                    <span>5</span>
                    товара на
                    <span class="basket_totalsum">1080,12 <span class="rub">a</span></span>
                  </a>
                  <div class="drop_down_type drop_down_right">
                    <form class="dropdown_form">
                      <table class="basket_tb">
                        <tr>
                          <td class="al_left">
                            <span class="bold">MAX232ACP+</span>
                            <span class="d_block op50">Maxim Integrated</span>
                          </td>
                          <td>
                            <span class="basket_num">3шт x</span>
                            <span class="basket_price">8,44<span class="rub">a</span></span>
                            <span class="bold d_block basket_sum semi_bold">25,32<span class="rub">a</span></span>
                          </td>
                          <td>
                            <span class="basket_closed"></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="al_left">
                            <span class="bold">ADM232AANZ</span>
                            <span class="d_block op50">Texas Instruments</span>
                          </td>
                          <td>
                            <span class="basket_num">30шт x</span>
                            <span class="basket_price">16,90<span class="rub">a</span></span>
                            <span class="bold d_block basket_sum semi_bold">507<span class="rub">a</span></span>
                          </td>
                          <td>
                            <span class="basket_closed"></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="al_left">
                            <span class="bold">ADM232ARNZ</span>
                            <span class="d_block op50">Analog Device</span>
                          </td>
                          <td>
                            <span class="basket_num">3шт x</span>
                            <span class="basket_price">8,44<span class="rub">a</span></span>
                            <span class="bold d_block basket_sum semi_bold">25,32<span class="rub">a</span></span>
                          </td>
                          <td>
                            <span class="basket_closed"></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="al_left">
                            <span class="bold">MAX232ACP+</span>
                            <span class="d_block op50">Linear Technology</span>
                          </td>
                          <td>
                            <span class="basket_num">3шт x</span>
                            <span class="basket_price">8,44<span class="rub">a</span></span>
                            <span class="bold d_block basket_sum semi_bold">25,32<span class="rub">a</span></span>
                          </td>
                          <td>
                            <span class="basket_closed"></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="al_left">
                            <span class="bold">MAX232ACP+</span>
                            <span class="d_block op50">Maxim Integrated</span>
                          </td>
                          <td>
                            <span class="basket_num">3шт x</span>
                            <span class="basket_price">8,44<span class="rub">a</span></span>
                            <span class="bold d_block basket_sum semi_bold">25,32<span class="rub">a</span></span>
                          </td>
                          <td>
                            <span class="basket_closed"></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="al_left">
                            <span class="bold">MAX232ACP+</span>
                            <span class="d_block op50">Analog Device</span>
                          </td>
                          <td>
                            <span class="basket_num">3шт x</span>
                            <span class="basket_price">8,44<span class="rub">a</span></span>
                            <span class="bold d_block basket_sum semi_bold">25,32<span class="rub">a</span></span>
                          </td>
                          <td>
                            <span class="basket_closed"></span>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="al_right">
                            <span class="d_ib semi_bold">Итого:</span>
                            <span class="d_ib semi_bold">1080,12<span class="rub">a</span></span>
                          </td>
                        </tr>
                      </table>
                      <div class="basket_box_links">
                        <button class="print_btn">b</button>
                        <button class="white_btn1">Перейти в корзину</button>
                        <button class="red_btn">Оформить заказ</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </header>