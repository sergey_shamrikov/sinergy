<footer class="bg_black">
  <div class="container-fluid">
    <div class="row mb40">
      <div class="col-lg-6 col-md-6">
        <h3 class="title2 mb10">Контактная информация</h3>
          <div class="contacts">
            <div>
              <span>Юридический адрес:</span><span>195196 Санкт-Петербург, Таллинская ул., 5-А, офис 301</span>
            </div>
            <div>
              <span>Фактический адрес:</span><span>195112 Санкт-Петербург, Новочеркасский пр., 40, офис 41</span>
            </div>
            <div>
              <span>Работаем</span><span>с 9:00 до 18:00 с понедельника по пятницу</span>
            </div>
            <div>
              <span>Телефон:</span><span>+7 812 385 76 47 <a href="#" class="black_btn">Перезвоните мне</a></span>
            </div>
            <div>
              <span>Почта:</span><span><a href="mailto:info@sinergy.ru" class="whiteUnder_lk">info@sinergy.ru</a></span>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <h3 class="title2 mb10">Наши реквизиты</h3>
          <div class="contacts">
            <div class="d_ib contacts_item p_left_zero">
              <span class="d_ib">Корр. счет:</span> <span class="d_ib">30101810100000000716</span>
            </div>
            <div class="d_ib contacts_item">
              <span class="d_ib">ИНН:</span> <span class="d_ib">7710353606</span>
            </div>
            <div class="d_ib contacts_item">
              <span class="d_ib">БИК:</span> <span class="d_ib">044525716</span>
            </div>
            <div class="d_ib contacts_item">
              <span class="d_ib">Код ОКПО:</span> <span class="d_ib">20606880</span>
            </div>
            <div class="d_ib contacts_item">
              <span class="d_ib">Код ОКОНХ:</span> <span class="d_ib">96120</span>
            </div>
            <div class="d_ib contacts_item p_left_zero">
              <span class="d_ib">ОГРН:</span> <span class="d_ib">1027739207462</span>
            </div>
            <div class="d_ib contacts_item">
              <span class="d_ib">КПП:</span> <span class="d_ib">775001001</span>
            </div>
          </div>
        </div>
      </div>
      <div class="footer_bottom row">
        <div class="col-lg-6 col-md-6">
          <p>© 2014 Синергия — интернет магазин ЭБК</p>
        </div>
        <div class="clearfix col-lg-6 col-md-6">
          <ul class="footer_list f_right">
            <li class="footer_list_item p_left_zero">
              <a href="#" class="footer_list_link">Политика конфеденциальности</a>
            </li>
            <li class="footer_list_item">
              <a href="#" class="footer_list_link">Публичная оферта</a>
            </li>
            <li class="footer_list_item">
              <a href="#" class="footer_list_link">Вакансии</a>
            </li>
            <li class="footer_list_item">
              <a href="#" class="footer_list_link">Контакты</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <script type="text/javascript" src="js/jquery.equalheights.js"></script>
  <script type="text/javascript" src="js/easyResponsiveTabs.js"></script>
  <script type="text/javascript" src="js/maskedInput.js"></script>
  <script type="text/javascript" src="js/ion.rangeSlider.js"></script>
  <script type="text/javascript" src="js/moment.min.js"></script>
  <script type="text/javascript" src="js/jquery.mCustomScrollbar.min.js"></script>
  <script type="text/javascript" src="js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="js/glDatePicker.min.js"></script>
  <script src="js/script.js"></script>
