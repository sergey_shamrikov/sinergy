﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ordering payer</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body class="">
  
<!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
<!--==============================content================================-->
  <div id="content">
    <div class="sh_box1">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <h2 class="title1 search_title">Оформление заказа</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="ordering_box">
      <div class="container-fluid">
          <ul class="ordering_delivery_list">
            <li class="ordering_list_item1"><a href="ordering_delivery.php" class="ordering_list_lk">Доставка</a></li>
            <li class="ordering_list_item2"><a href="ordering_payer.php" class="ordering_list_lk">Плательщик</a></li>
            <li class="ordering_list_item3 current"><a href="ordering_order.php" class="ordering_list_lk">Заказ</a></li>
            <li class="ordering_list_item3"><a href="ordering_payment.php" class="ordering_list_lk">Оплата</a></li>
          </ul>
      </div>
      <div class="bg_white">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-6 col-lg-offset-2">
              <span class="d_block pb26">Пожалуйста, проверьте ваш заказ перед тем, как закончить оформление.</span>
            </div>
          </div>
          <form>
            <div class="row">
              <div class="col-lg-11">
                <div class="ordering_data_box wrapper">
                  <div class="form_box wrapper mb4">
                    <span class="f_left ordering_data">Покупатель</span>
                    <span class="f_left d_ib bold">Юридическое лицо</span>
                  </div>
                  <div class="form_box wrapper mb4">
                    <span class="f_left ordering_data">Компания</span>
                    <span class="f_left d_ib">Рога и копыта</span>
                  </div>
                  <div class="form_box wrapper mb4">
                    <span class="f_left ordering_data">Телефон</span>
                    <span class="f_left d_ib">+7 965 123 45 67</span>
                  </div>
                  <div class="form_box wrapper mb4">
                    <span class="f_left ordering_data">Адрес юр.</span>
                    <span class="f_left d_ib">432072 г.Юльяновск, пр.Юльяновский 71, офис 22</span>
                  </div>
                  <div class="form_box wrapper mb4">
                    <span class="f_left ordering_data">ИНН</span>
                    <span class="f_left d_ib">3667 2211 2501</span>
                  </div>
                  <div class="form_box wrapper mb20">
                    <span class="f_left ordering_data">КПП</span>
                    <span class="f_left d_ib">2501 2553 2502 2504</span>
                  </div>
                  <div class="form_box wrapper mb20">
                    <span class="f_left ordering_data">Ваш заказ</span>
                    <div class="f_left d_ib bold">
                      <div>
                        <span class="bold d_ib mb4">MAX232ACP+</span><span class="d_ib ordering_data_sum"> 268 шт.</span>
                      </div>
                      <div>
                        <span class="bold d_ib mb4">ADM232AANZ</span><span class="d_ib ordering_data_sum"> 47 шт.</span>
                      </div>
                      <div>
                        <span class="bold d_ib">MAX232AARN</span><span class="d_ib ordering_data_sum"> 18 шт.</span>
                      </div>
                    </div>
                  </div>
                  <div class="form_box wrapper mb20">
                    <span class="f_left ordering_data">Доставка</span>
                    <div class="f_left">
                      <span class="d_ib bold">Самовывоз</span> <span class="d_ib">из офиса в Санкт-Петербурге</span>
                    </div>
                  </div>
                   <div class="form_box wrapper mb25">
                    <span class="f_left ordering_data">Сроки поставки</span>
                    <span class="f_left d_ib">Поставка на склад в течении 2-3 недель, вы можете забрать заказ частями, мы сообщим Вам по эл. почте о поставке товара.</span>
                  </div>
                </div>
             </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-lg-offset-2 pb20">
              <h3 class="title4 bold">Итого с учётом скидки и доставки: <span class="d_ib">49 327,88 7</span><span class="rub">a</span></h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 al_right">
                <span class="d_ib gray mr-15">Дополнительно</span>
              </div>
              <div class="col-lg-9 pb13">
                <div class="d_ib w15">
                  <input type="text" value="Номер заказа" onblur="if(this.value=='') this.value='Номер заказа'" onfocus="if(this.value =='Номер заказа' ) this.value=''">
                </div>
                <div class="d_ib w15">
                  <input type="text" value="Дата заказа" onblur="if(this.value=='') this.value='Дата заказа'" onfocus="if(this.value =='Дата заказа' ) this.value=''">
                </div>
                <span class="d_ib form_box_subscrib">Данные из вашей учётной системы заказов (указываются опционально)</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-lg-offset-2">
                <div class="form_box">
                  <input type="checkbox" id="consent" class="d_ib">
                  <label class="form_box_lb" for="consent"> С правилами оплаты и доставки ознакомился и согласен</label>
                    <span class="d_block gray links_data_lk">Посмотреть<a href="#" target="_blank" class="gray black_lk">условия оплаты, </a><a href="#" target="_blank" class="gray black_lk"> способы доставки</a> и <a href="#" target="_blank" class="gray black_lk">договор-оферты</a>, ссылки откроются в отдельном окнe</span>
                </div>
                <div class="box_order_links pb20">
                  <button class="red_btn">Завершить оформление и скачать квитанцию для оплаты</button>
                  <a href="#" class="black_lk val_mid">Назад</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>