﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Contacts</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
<!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
<!--==============================content================================-->
  <div id="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Интегральные микросхемы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Аудио</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Предохранители, фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Карты и модули памяти</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Реле</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Трансформаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Конденсаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Датчики, сенсоры</a>
                  <ul class="sub_catalog_list">
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Аксессуары</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Модульные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Пленочные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Танталовые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                    </li>
                  </ul>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Дискретные полупроводники</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Разъемы, соединители</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <h2 class="title1 catalog_user">Контактная информация</h2>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-4 col-lg-offset-2">
           <div class="contacts_2">
            <div>
              <span>Юридический адрес:</span><span>195196 Санкт-Петербург, Таллинская ул., 5-А, офис 301</span>
            </div>
            <div>
              <span>Фактический адрес:</span><span>195112 Санкт-Петербург, Новочеркасский пр., 40, офис 41</span>
            </div>
            <div>
              <span>Работаем</span><span>с 9:00 до 18:00 с понедельника по пятницу</span>
            </div>
            <div>
              <span>Телефон:</span><span>+7 812 385 76 47</span>
            </div>
            <div>
              <span>Почта:</span><span><a href="mailto:info@sinergy.ru" class="black_lk">info@sinergy.ru</a></span>
            </div>
          </div>
          <h2 class="title1 catalog_user">Наши реквизиты</h2>
          <div class="contacts_2 pb45">
            <div class="contacts_item">
              <span class="d_ib">Корр. счет:</span> <span class="d_ib">30101810100000000716</span>
            </div>
            <div class="contacts_item">
              <span class="d_ib">ИНН:</span> <span class="d_ib">7710353606</span>
            </div>
            <div class="contacts_item">
              <span class="d_ib">Код ОКПО:</span> <span class="d_ib">20606880</span>
            </div>
            <div class="contacts_item">
              <span class="d_ib">ОГРН:</span> <span class="d_ib">1027739207462</span>
            </div>
          </div>
          </div>
        </div>
      </div>
      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d43569.40913881983!2d32.00078722216798!3d46.9599595876876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1421677609412" height="494"></iframe>
      </div>
  </div>
<!--==============================footer=================================-->
 <?php include("main_blocks/footer.php") ?>
   
</body>
</html>