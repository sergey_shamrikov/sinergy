﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>producer</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="container-fluid">
       <div class="row">
        <div class="col-lg-2 p_rel">
          <div class="catalog_box catalog_hidden">
            <h5 class="catalog_title">Каталог</h5>
            <ul class="catalog_list">
              <li class="catalog_item">
                <a href="#" class="catalog_link">Интегральные микросхемы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Аудио</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Предохранители, фильтры</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Фильтры</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Карты и модули памяти</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Реле</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Радиочастотные компоненты</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Трансформаторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Конденсаторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Датчики, сенсоры</a>
                <ul class="sub_catalog_list">
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Аксессуары</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Модульные</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Пленочные</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Танталовые</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                  </li>
                </ul>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Дискретные полупроводники</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Разъемы, соединители</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <h3 class="title1 catalog_user pl163">Производители</h3>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row mb48">
        <div class="col-lg-3">
          <ul class="manufacture_list" data-letter="a">
            <li class="manufacture_list_item">
              <a href="#">ACP</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Aries Electronics</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Atmel</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">AVX</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="b">
            <li class="manufacture_list_item">
              <a href="#">Bolymin</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Bourns</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="c">
            <li class="manufacture_list_item">
              <a href="#">Cosmo</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">CT Concept</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="d">
            <li class="manufacture_list_item">
              <a href="#">Dau</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="e">
            <li class="manufacture_list_item">
              <a href="#">E2V</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Epcos</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Evercool</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="manufacture_list" data-letter="f">
            <li class="manufacture_list_item">
              <a href="#">Freescale</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="g">
            <li class="manufacture_list_item">
              <a href="#">Gemalto</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="h">
            <li class="manufacture_list_item">
              <a href="#">Hirose</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Hitano Enterprise Corporation</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">HUBER+SUHNER AG</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="i">
            <li class="manufacture_list_item">
              <a href="#">Infineon Technologies</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">International Rectifier</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Isabellenhutte</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">iWave</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="k">
            <li class="manufacture_list_item">
              <a href="#">Kendeil</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="l">
            <li class="manufacture_list_item">
              <a href="#">Lantiq</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Lightech</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="manufacture_list" data-letter="m">
            <li class="manufacture_list_item">
              <a href="#">Maxim Integrated</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Microchip Technology</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Mitsubishi Electric IGBT</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Molex</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Murata</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Murata Power Solutions</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="n">
            <li class="manufacture_list_item">
              <a href="#">Nichicon</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Nordic</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="o">
            <li class="manufacture_list_item">
              <a href="#">Omron</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="p">
            <li class="manufacture_list_item">
              <a href="#">ParaLight</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="r">
            <li class="manufacture_list_item">
              <a href="#">Redpine Signals</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Rohm  IRDA</a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="manufacture_list" data-letter="s">
            <li class="manufacture_list_item">
              <a href="#">Samsung</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Seoul Semiconductor</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Switronic</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="t">
            <li class="manufacture_list_item">
              <a href="#">TDK-Lambda</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">TTI</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="v">
            <li class="manufacture_list_item">
              <a href="#">Vishay</a>
            </li>
          </ul>
          <ul class="manufacture_list" data-letter="w">
            <li class="manufacture_list_item">
              <a href="#">Wells-CTI</a>
            </li>
            <li class="manufacture_list_item">
              <a href="#">Wurth</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>