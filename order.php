﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Order</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="sh_box">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Интегральные микросхемы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Аудио</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Предохранители, фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Карты и модули памяти</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Реле</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Трансформаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Конденсаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Датчики, сенсоры</a>
                  <ul class="sub_catalog_list">
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Аксессуары</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Модульные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Пленочные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Танталовые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                    </li>
                  </ul>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Дискретные полупроводники</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Разъемы, соединители</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-6">
            <h2 class="title1 search_title">Заказ № 21132</h2>
          </div>
          <div class="col-lg-4 al_right">
            <div class="d_ib">
              <div class="personal_meneger d_tb">
                <div class="d_tc"><img src="images/meneger.png" height="48" width="48" alt="picture"></div>
                <div class="d_tc pers_box_right al_left">
                  <span class="pers_men_name">Ваш личный менеджер: Василиса</span>
                  <span class="d_in-block pers_men_tel">Тел.: +7 812 923 25 29</span>
                  <span class="d_in-block pers_men_post">Почта:<a href="mailto:vasilisa@sinergy.ru">vasilisa@sinergy.ru</a></span>
                  <div class="al_just pers_men_inner drop_down_box">
                    <button class="white_btn">Заказать звонок</button>
                    <button class="white_btn drop_down_btn bdr5">Написать менеджеру</button>
                    <div class="drop_down drop_down_right">
                  <form class="dropdown_form p_rel">
                    <ul class="meneg_mess_main">
                      <li class="meneg_mess_item">
                        <h6>Василиса в 13:37</h6>
                        <p>
                          Добрый день! У вас возникли вопросы?
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Вы в 13:37</h6>
                        <p>
                          Мне нужна выписка для налоговой.
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Василиса в 13:38</h6>
                        <p>
                          Да конечно, оставьте свой адрес вашей эл.почты, в течение 20 минут я вышлю вам всё необходимое.
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Вы в 13:42</h6>
                        <p>
                          Большое спасибо, вот почта: vasja@pupkin.ru
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Василиса в 13:59</h6>
                        <p>
                          Отправила выписку на почту, пожалуйста, проверьте.
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Вы в 13:42</h6>
                        <p>
                          Ещё раз спасибо, всё получил!
                        </p>
                      </li>
                    </ul>
                    <textarea class="mb6" onblur="if(this.value=='') this.value='Комментарий'" onfocus="if(this.value =='Комментарий' ) this.value=''">Сообщение</textarea>
                    <div class="al_right">
                      <button class="red_btn">Отправить</button>
                    </div>
                  </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>
        <div class="row mb30">
          <div class="col-alex-4 col-lg-9  mt-20">
            <ul class="inf_about_order_list">
              <li class="inf_about_order_list_item clearfix">
                <div class="">Дата заказа</div>
                <div class="">21.11.14 19:29</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">Статус заказа</div>
                <div class="">Оформляется</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">Сумма заказа</div>
                <div class="">140 320,60 ₽</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">Статус оплаты</div>
                <div class=" bold">Не оплачен</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">Вид оплаты</div>
                <div class="">Картой Visa или MasterCard</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">Дата оплаты</div>
                <div class="">-</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">№ платежного получения</div>
                <div class="">-</div>
              </li>
              <li class="inf_about_order_list_item clearfix">
                <div class="">Трекинг номер</div>
                <div class="">-</div>
              </li>
            </ul>
          </div>
          <div class="col-alex-5 col-lg-8 mt-20_1400">
            <ul class="form_booking">
              <li class="form_booking_item mb8 clearfix">
                <label class="form_booking_label">Внутренний номер</label>
                <span class="d_in-block w52"><input class="form_booking_input" type="text"></span>
              </li>
              <li class="form_booking_item mb8 clearfix">
                <label class="form_booking_label">Внутренняя дата</label>
                <span class="d_in-block w52"><input class="form_booking_input" type="text"></span>
              </li>
              <li class="form_booking_item mb8 clearfix">
                <label class="form_booking_label">Вид доставки</label>
                <div class="f_left w84 mr-40 gray3 custom_select main_select">
                  <select class="mb6">
                    <option>Доставка по стране почтовой службой</option>
                    <option>Доставка по стране почтовой службой2</option>
                    <option>Доставка по стране почтовой службой3</option>
                    <option>Доставка по стране почтовой службой4</option>
                  </select>
                </div>
              </li>
              <li class="form_booking_item mb8 clearfix">
                <label class="form_booking_label">Адрес доставки</label>
                <input class="form_booking_input w84 mr-40" type="text" value="432072, г. Ульяновск, пр-т. Ульяновский 21, офис 2" onblur="if(this.value=='') this.value='432072, г. Ульяновск, пр-т. Ульяновский 21, офис 2'" onfocus="if(this.value =='432072, г. Ульяновск, пр-т. Ульяновский 21, офис 2' ) this.value=''">
              </li>
              <li class="form_booking_item mb8 clearfix">
                <label class="form_booking_label">Контакт для связи</label>
                <span class="w52 f_left"><input class="form_booking_input" type="text" value="Василий Александрович Пупкин" onblur="if(this.value=='') this.value='Василий Александрович Пупкин'" onfocus="if(this.value =='Василий Александрович Пупкин' ) this.value=''"></span>
                <input type="tel" class="phone form_booking_input ml8 mr-20 w26 p_rel">
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <h2 class="title1 al_center">Состав заказа</h2>
          <table class="sinergia_order_table mt20">
              <tbody>
                <tr>
                  <th class="col3 w1">
                    <input type="checkbox" id="order1" class="d_ib">
                    <label class="form_box_lb" for="order1"></label>
                  </th>
                  <th class="col3 w1">№</th>
                  <th class="col3"><span class="tab_aft_dwn">Наименование</span></th>
                  <th class="col3">Производитель</th>
                  <th class="col3">Описание</th>
                  <th class="col3">Кол-во</th>
                  <th class="col3">Цена</th>
                  <th class="col3">Сумма</th>
                  <th class="col3">Статус</th>
                  <th class="col3">Поставщик</th>
                  <th class="col3">Артикул</th>
                  <th class="col3">Склад</th>
                  <th class="col4">Срок <br>поставки</th>
                  <th class="col3">№ и дата <br>реализации</th>
                  <th></th>
                </tr>
                <tr>
                  <td class="w1">
                    <input type="checkbox" id="order2" class="d_ib">
                    <label class="form_box_lb" for="order2"></label>
                  </td>
                  <td class="w1">1</td>
                  <td class="bold">MAX232ACPE+</td>
                  <td>Maxim Integrated</td>
                  <td> 
                    <span class="d_block">Standard Circular Connector </span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td>
                    <div class="d_ib w60p">
                      <input type="text" class="d_ib">
                    </div>
                  </td>
                  <td>320,60<span class="rub"> a</span></td>
                  <td>24 320,60<span class="rub"> a</span></td>
                  <td> 
                    <span class="d_block">Отгружено: 100</span>
                    <span class="d_block">На складе: 231</span>
                    <span class="d_block">Доставляется: 210</span>
                    <a class="link_ord_table" href="#">Подробнее</a>
                  </td>
                  <td>Maxim Integrated</td>
                  <td>A1303-ND</td>
                  <td>
                    <span class="d_block">3968 шт</span>
                    <span class="d_block">Мин. заказ: 1 шт.</span>
                    <span class="d_block">Кратность: 1 шт.</span>
                  </td>
                  <td>
                    <span class="d_block">4 недели</span> 
                    <span class="d_block">03.02.15</span>
                  </td>
                  <td>
                    <span class="d_block">№ 23143</span>
                    <span class="d_block">03.02.15</span>
                  </td>
                  <td><a href="javascript:;" class="close_table delete_btn"></a></td>
                </tr>
                <tr>
                  <td class="w1">
                    <input type="checkbox" id="order3" class="d_ib">
                    <label class="form_box_lb" for="order3"></label>
                  </td>
                  <td class="w1">2</td>
                  <td class="bold">ADM232AANZ</td>
                  <td>Texas Instruments</td>
                  <td> 
                    <span class="d_block">Standard Circular Connector </span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td>
                    <div class="d_ib w60p">
                      <input type="text" class="d_ib">
                    </div>
                  </td>
                  <td>320,60<span class="rub"> a</span></td>
                  <td>24 320,60<span class="rub"> a</span></td>
                  <td> 
                    <span class="d_block">Отгружено: 100</span>
                    <span class="d_block">На складе: 231</span>
                    <span class="d_block">Доставляется: 210</span>
                    <a class="link_ord_table" href="#">Подробнее</a>
                  </td>
                  <td>Texas Instruments</td>
                  <td>A1303-ND</td>
                  <td>
                    <span class="d_block">3968 шт</span>
                    <span class="d_block">Мин. заказ: 1 шт.</span>
                    <span class="d_block">Кратность: 1 шт.</span>
                  </td>
                  <td>
                    <span class="d_block">4 недели</span> 
                    <span class="d_block">03.02.15</span>
                  </td>
                  <td>
                    <span class="d_block">№ 23143</span>
                    <span class="d_block">03.02.15</span>
                  </td>
                  <td><a href="javascript:;" class="close_table delete_btn"></a></td>
                </tr>
                <tr>
                  <td class="w1">
                    <input type="checkbox" id="order4" class="d_ib">
                    <label class="form_box_lb" for="order4"></label>
                  </td>
                  <td class="w1">3</td>
                  <td class="bold">ADM232AARN</td>
                  <td>Analog device</td>
                  <td> 
                    <span class="d_block">Standard Circular Connector </span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td>
                    <div class="d_ib w60p">
                      <input type="text" class="d_ib">
                    </div>
                  </td>
                  <td>320,60<span class="rub"> a</span></td>
                  <td>24 320,60<span class="rub"> a</span></td>
                  <td> 
                    <span class="d_block">Отгружено: 100</span>
                    <span class="d_block">На складе: 231</span>
                    <span class="d_block">Доставляется: 210</span>
                    <a class="link_ord_table" href="#">Подробнее</a>
                  </td>
                  <td>Analog device</td>
                  <td>A1303-ND</td>
                  <td>
                    <span class="d_block">3968 шт</span>
                    <span class="d_block">Мин. заказ: 1 шт.</span>
                    <span class="d_block">Кратность: 1 шт.</span>
                  </td>
                  <td>
                    <span class="d_block">4 недели</span> 
                    <span class="d_block">03.02.15</span>
                  </td>
                  <td>
                    <span class="d_block">№ 23143</span>
                    <span class="d_block">03.02.15</span>
                  </td>
                  <td><a href="javascript:;" class="close_table delete_btn"></a></td>
                </tr>
              </tbody>
            </table>
            <div class="button_under_tab clearfix">
              <div class="f_left left_col_bt">
                <button class="white_bd_lk_2">Удалить выбранный</button>
              </div>
              <div class="f_right right_col_bt">
                <div class="butn_box">
                  <button class="white_btn1 print_but">Распечатать</button>
                </div>
                <div class="butn_box">
                  <button class="white_btn1">Экспортировать в Excel</button>
                </div>
                <div class="butn_box">
                  <button class="white_btn1">Отменить заказ</button>
                </div>
                <div class="butn_box">
                  <button class="black2_btn_2">Редактировать</button>
                </div>
                <div class="drop_down_box butn_box">
                  <button class="red_btn drop_down_btn">Оплатить</button>
                  <div class="drop_down drop_down_right">
                    <form class="dropdown_form">
                      <ul class="cash">
                        <li class="cash_item">
                          <a href="#" class="cash_link">Безналичным переводом</a>
                          <p class="cash_day">1-3 рабочих дня</p>
                        </li>
                        <li class="cash_item">
                          <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                          <p class="cash_day">Мгновенно и без комиссий</p>
                        </li>
                        <li class="cash_item">
                          <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                          <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                        </li>
                        <li class="cash_item">
                          <a href="#" class="cash_link">Через интернет-банк</a>
                          <p class="cash_day">Без комиссий</p>
                        </li>
                        <li class="cash_item">
                          <a href="#" class="cash_link">В салонах мобильной связи</a>
                          <p class="cash_day">Может взиматься комиссия</p>
                        </li>
                        <li class="cash_item">
                          <a href="#" class="cash_link">Через платежные терминалы</a>
                          <p class="cash_day">Может взиматься комиссия</p>
                        </li>
                      </ul>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>