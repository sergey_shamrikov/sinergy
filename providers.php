﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Payment_histopy</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="sh_box">
      <div class="container-fluid">
         <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Интегральные микросхемы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Аудио</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Предохранители, фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Карты и модули памяти</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Реле</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Трансформаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Конденсаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Датчики, сенсоры</a>
                  <ul class="sub_catalog_list">
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Аксессуары</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Модульные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Пленочные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Танталовые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                    </li>
                  </ul>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Дискретные полупроводники</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Разъемы, соединители</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <h2 class="title1 catalog_user">Поставщики</h2>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <ul class="provider_list clearfix">
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.fairchild.com" class="provider_link">
                  <img src="images/farnell.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title short_inf_title semi_bold"><a href="#">Premier Farnell plc.</a></h3>
                <p>
                  Farnell является ведущим мировым складским дистрибьютором электронных компонентов, приборов, инструмента, расходных и технологических материалов для электронной промышленности. Компания была основана в 1939 году в Великобритании.                     
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.fairchild.com">www.fairchild.com</a>
                </div>
              </div>
            </div>
          </li>
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.digikey.com" class="provider_link">
                  <img src="images/digi-key.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title semi_bold"><a href="#">Digi-Key Corporation</a></h3>
                <p>
                  Основанная в 1972 году, корпорация DigiKey по праву считается лидером поставок электронных компонентов на территории США. Сотни тысяч компонентов на складе, всегда высокое качество и одни из самых низких цен на розничные товары.
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.digikey.com">www.digikey.com</a>
                </div>
              </div>
            </div>
          </li>
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.mouser.com" class="provider_link">
                  <img src="images/mouser.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title semi_bold"><a href="#">Mouser Electronics</a></h3>
                <p>
                  Каталог Mouser – один из крупнейших мировых поставщиков радиоэлектронных компонентов. Розничное подразделение корпорации TTI. Основанная в 1964 году в Калифорнии компания сейчас имеет офисы по всему миру и гигантский каталог компонентов с приятными ценами.
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.mouser.com">www.mouser.com</a>
                </div>
              </div>
            </div>
          </li>
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.avnet.com" class="provider_link">
                  <img src="images/avnet.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title semi_bold"><a href="#">Avnet, Inc.</a></h3>
                <p>
                  Корпорация Avnet включает в себя ряд крупнейших европейских и американских поставщиков, дочерними предприятиями компании являются: EBV-Elektronik, Silica, Avnet Memec и Avnet Abacus. 
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.avnet.com">www.avnet.com</a>
                </div>
              </div>
            </div>
          </li>
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.alliedelec.com" class="provider_link">
                  <img src="images/allied.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title semi_bold"><a href="#">Allied Electronics</a></h3>
                <p>
                  Крупный поставщик США, ориентирован на рынки США и Канады, имеет более 50 торговых представительств. Специализируется на пассивных и активных электронных компонентах, разьёмах, реле, электротехнических компонентах.
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.alliedelec.com">www.alliedelec.com</a>
                </div>
              </div>
            </div>
          </li>
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.tme.eu" class="provider_link">
                  <img src="images/tem.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title semi_bold"><a href="#">TME</a></h3>
                <p>
                  Transfer Multisort Elektronik была создана в 1990, является крупнейшим поставщикм электронных элементов в Центральной и Восточной Европе. Центральный склад компании находится в Польше и насчитывает более 50000 наименований.
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.tme.eu">www.tme.eu</a>
                </div>
              </div>
            </div>
          </li>
          <li class="providers_list_item row">
            <div class="col-lg-2">
              <div class="provider-logo">
                <a href="http://www.rs-online.com" class="provider_link">
                  <img src="images/rs.png" alt="">
                </a>
              </div>
            </div>
            <div class="col-lg-10">
              <div class="provider_short_inf">
                <h3 class="short_inf_title semi_bold"><a href="#">RS Components</a></h3>
                <p>
                  Компания RS Components образована в 1937 году и является лидером в Европе по поставке электронных, электрических и механических компонентов и сопутствующих товаров. В настоящее время открыты представительства в 25 странах.
                </p>
                <div class="link_item">
                  <a class="www_lk" href="http://www.rs-online.com">www.rs-online.com</a>
                </div>
              </div>
            </div>
          </li>
        </ul>
        <div class="row">
          <div class="col-lg-12">
            <div class="prov_btn_show_more">
              <a href="javascript:;" class="white_bd_lk">Показать еще</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
   <?php include("main_blocks/footer.php") ?>
   
</body>
</html>