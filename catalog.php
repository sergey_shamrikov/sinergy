﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Catalog</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>

 
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="sh_box">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-2 p_rel">
          <div class="catalog_box catalog_hidden"></div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <h2 class="title1 catalog_user">Каталог</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h3 class="title4 semi_bold pb20">Полупроводниковые приборы: 1765</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Встроенные процессоры и контроллеры</a>
              </p>
              <span class="p_abs">765</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Интегральные схемы - ИС</a>
              </p>
              <span class="p_abs">110</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">РЧ полупроводники</a>
              </p>
              <span class="p_abs">138</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Дискретные полупроводниковые приборы</a>
              </p>
              <span class="p_abs">2196</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h3 class="title4 semi_bold pb20">Встроенные решения: 12 148</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">USB-флэш-накопители</a>
              </p>
              <span class="p_abs">512</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Инструментальные средства</a>
              </p>
              <span class="p_abs">768</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули интерфейсов</a>
              </p>
              <span class="p_abs">119</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Аудиомодули</a>
              </p>
              <span class="p_abs">104</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Карты памяти</a>
              </p>
              <span class="p_abs">326</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули памяти</a>
              </p>
              <span class="p_abs">110</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Беспроводные/радиочастотные модули</a>
              </p>
              <span class="p_abs">612</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Комплектующие для модулей</a>
              </p>
              <span class="p_abs">83</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули сбора энергии</a>
              </p>
              <span class="p_abs">5079</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Видеомодули</a>
              </p>
              <span class="p_abs">47</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули визуального вывода</a>
              </p>
              <span class="p_abs">47</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули управления питанием</a>
              </p>
              <span class="p_abs">9128</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Вычислительная техника</a>
              </p>
              <span class="p_abs">84</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули датчиков</a>
              </p>
              <span class="p_abs">19</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Сетевые (Ethernet) и коммуникационные модули</a>
              </p>
              <span class="p_abs">14</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Другие модули</a>
              </p>
              <span class="p_abs">2196</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Модули для преобразования данных</a>
              </p>
              <span class="p_abs">702</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Твердотельные накопители (SSD)</a>
              </p>
              <span class="p_abs">1482</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h3 class="title4 semi_bold pb20">Датчики:4083</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Аудиодатчики</a>
              </p>
              <span class="p_abs">512</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики состояния окружающей среды</a>
              </p>
              <span class="p_abs">768</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Кодеры</a>
              </p>
              <span class="p_abs">119</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Биомедицинские датчики</a>
              </p>
              <span class="p_abs">104</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики тока</a>
              </p>
              <span class="p_abs">326</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Линейные датчики перемещения</a>
              </p>
              <span class="p_abs">110</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики давления</a>
              </p>
              <span class="p_abs">612</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики уровня жидкости</a>
              </p>
              <span class="p_abs">83</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Магнитные датчики</a>
              </p>
              <span class="p_abs">5079</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики движения и позиционирования</a>
              </p>
              <span class="p_abs">47</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Емкостные датчики касания</a>
              </p>
              <span class="p_abs">47</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Оптические датчики</a>
              </p>
              <span class="p_abs">9128</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики потока</a>
              </p>
              <span class="p_abs">84</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Защитные световые завесы</a>
              </p>
              <span class="p_abs">19</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Средства разработки датчиков</a>
              </p>
              <span class="p_abs">14</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Датчики расстояния</a>
              </p>
              <span class="p_abs">2196</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Измерительное оборудование и принадлежности</a>
              </p>
              <span class="p_abs">702</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Температурные датчики</a>
              </p>
              <span class="p_abs">1482</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h3 class="title4 semi_bold pb20">Защита от замыкания: 217</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">TVS–диоды</a>
              </p>
              <span class="p_abs">768</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Защитные устройства для светодиодов</a>
              </p>
              <span class="p_abs">119</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Подавители статического заряда</a>
              </p>
              <span class="p_abs">104</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Автоматические выключатели и принадлежности</a>
              </p>
              <span class="p_abs"></span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Комплекты для защиты цепей</a>
              </p>
              <span class="p_abs">612</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Сетевые удлинители</a>
              </p>
              <span class="p_abs">83</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Варисторы Патроны плавких предохранителей</a>
              </p>
              <span class="p_abs">5079</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Термисторы</a>
              </p>
              <span class="p_abs">47</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Восстанавливаемые предохранители - PPTC</a>
              </p>
              <span class="p_abs">47</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Плавкие предохранители</a>
              </p>
              <span class="p_abs">9128</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Термические защитные выключатели</a>
              </p>
              <span class="p_abs">19</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Газоразрядные трубки / газоплазменные разрядники</a>
              </p>
              <span class="p_abs">14</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Подавители выбросов напряжения</a>
              </p>
              <span class="p_abs">2196</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">Тиристоры</a>
              </p>
              <span class="p_abs">1482</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <h3 class="title4 semi_bold pb20">Инструментальные средства разработки: 217</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Аналоговые и цифровые средства разработки ИС</a>
              </p>
              <span class="p_abs">768</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР датчиков</a>
              </p>
              <span class="p_abs">4119</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР на радиочастотной/беспроводной основе</a>
              </p>
              <span class="p_abs">64</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">Программное обеспечение для разработки</a>
              </p>
              <span class="p_abs">612</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР ИС управления питанием</a>
              </p>
              <span class="p_abs">1083</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР оптоэлектроники</a>
              </p>
              <span class="p_abs">5079</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР волоконной оптики</a>
              </p>
              <span class="p_abs">9128</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР коммуникаций</a>
              </p>
              <span class="p_abs">1719</span>
            </li>
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР схем светодиодного освещения</a>
              </p>
              <span class="p_abs">14</span>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <ul class="components_list">
            <li class="list4_item p_rel">
              <p>
                <a href="#">СР встроенных компонентов</a>
              </p>
              <span class="p_abs">1482</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
    </div>
  </div>

  <!--==============================footer=================================-->
    <?php include("main_blocks/footer.php") ?>

</body>
</html>