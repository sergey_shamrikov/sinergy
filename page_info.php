﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Page_info</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="container-fluid">
       <div class="row">
        <div class="col-lg-2 p_rel">
          <div class="catalog_box catalog_hidden">
            <h5 class="catalog_title">Каталог</h5>
            <ul class="catalog_list">
              <li class="catalog_item">
                <a href="#" class="catalog_link">Интегральные микросхемы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Аудио</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Предохранители, фильтры</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Фильтры</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Карты и модули памяти</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Реле</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Радиочастотные компоненты</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Трансформаторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Конденсаторы</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Датчики, сенсоры</a>
                <ul class="sub_catalog_list">
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Аксессуары</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Модульные</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Пленочные</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Танталовые</a>
                  </li>
                  <li class="sub_catalog_item">
                    <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                  </li>
                </ul>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Дискретные полупроводники</a>
              </li>
              <li class="catalog_item">
                <a href="#" class="catalog_link">Разъемы, соединители</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <ul class="breadcrumbs clearfix">
              <li class="breadcrumbs_item d_in-block">
                <a href="sinergia_providers.html" class="breadcrumbs_link">
                  <span class="breadcrumbs_current">Главная</span>
                </a>
                /
              </li>
              <li class="breadcrumbs_item d_in-block">
                <span class="breadcrumbs_current">Оплата и доставка</span>
              </li>
            </ul>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row mb15">
        <div class="col-lg-2">
          <div class="provider-logo2 al_right">
            <img src="images/visa.png" alt="">
          </div>
        </div>
        <div class="col-lg-10">
          <h3 class="title1 prov_titel_item">Оплата</h3>
          <p>
            Наличный платёж — операции с денежной наличностью, которые проводятся физическими и юридическими лицами. Как правило, эти расчёты меньше по объёму по сравнению с безналичными расчётами. Размер наличного расчёта обычно устанавливается в законодательном порядке. К наличному расчёту относятся также выплаты предприятий, организаций, учреждений населению в виде заработной платы, стипендий, пенсий, пособий; поступления из финансовой системы; денежные расходы населения на товары и услуги; платежи финансовой системе. Государство в лице центрального банка и министерства финансов организует и контролирует налично-денежный оборот и расчёты по нему. В международной торговле расчёт наличными осуществляется чеками, переводами, аккредитивами, инкассо.
          </p>
          <p>
            Безналичный платёж — платёж, осуществляемый между продавцом и покупателем при помощи банка путём перечисления денежных средств с банковских счетов. Платёж электронными деньгами — платёж, осуществляемый при помощи платёжных инструментов электронных денег.
          </p>
          <p>
            Обязательный платёж — налоги, сборы и иные обязательные взносы, уплачиваемые в бюджет соответствующего уровня бюджетной системы Российской Федерации и (или) государственные внебюджетные фонды в порядке и на условиях, которые определяются законодательством Российской Федерации, в том числе штрафы, пени и иные санкции за неисполнение или ненадлежащее исполнение обязанности по уплате налогов, сборов и иных обязательных взносов в бюджет соответствующего уровня бюджетной системы Российской Федерации и (или) государственные внебюджетные фонды, а также административные штрафы и установленные уголовным законодательством штрафы[1].
          </p>
        </div>
      </div>
      <div class="row mb40">
        <div class="col-lg-2">
          <div class="provider-logo2 al_right">
              <img src="images/delivery.png" alt="">
          </div>
        </div>
        <div class="col-lg-10">
          <h3 class="title1 prov_titel_item">Доставка</h3>
          <p>
            Уайли работает в Нью-Йорке курьером-велосипедистом в фирме по срочной доставке. Однажды он получает заказ от Нимы, знакомой своей подруги Ванессы. Ванесса работает в той же фирме по срочной доставке и от неё Нима знает, что на Уайли можно положиться.
          </p>
          <p>
            В своё время Нима приехала из Китая по студенческой визе. В Китае остался её сын, которого она хотела после обустройства перевезти в Нью-Йорк, но по политическим мотивам китайские власти запретили ему выезд. Нима обратилась к «торговцу», нелегальному перевозчику людей. В качестве оплаты «торговец» принимает не наличные деньги, а «билет» — подобие денежного чека некоего «банка» — китайской нелегальной финансовой организации, базирующейся в Нью-Йорке. Нима кладёт в «банк» все свои деньги, получает «билет» и отдаёт его в запечатанном конверте Уайли, чтобы он к 19:00 доставил его «торговцу» в условленное место.
          </p>
          <p>
            В это же время полицейский Бобби Манди, завязший в карточных долгах, отрабатывает долг у китайских гангстеров. Ему дают поручение раздобыть «билет», ведь по нему гангстеры смогут изъять из «банка» все сданные Нимой деньги. Манди через Ванессу выходит на Ниму, через Ниму выходит на Уайли и в автомобиле гоняется за его велосипедом по всему маршруту доставки. По ходу действия фильма Уайли узнаёт подробности всей истории с «билетом», а в погоню в той или иной степени постепенно вовлекаются полицейский-велосипедист, Ванесса, ещё один курьер Мэнни (который конкурирует с Уайли и пытается брутально ухаживать за Ванессой), а также китайские гангстеры, полицейские и медики Нью-Йорка.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>