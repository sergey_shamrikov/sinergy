﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?> 
  <!--==============================content================================-->
    <div id="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-2 p_rel">
              <div class="catalog_box catalog_hidden">
                <h5 class="catalog_title">Каталог</h5>
                <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-8 col-md-8">
              <h2 class="title1 catalog_user">Регистрация нового пользователя</h2>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-5 col-lg-offset-1">
            <div class="ml_">
              <div class="form_box wrapper">
                <label class="f_left form_box_text">Логин<span class="red">*</span></label>
                <div class="w30 f_left">
                  <input type="text" class="mb6 p_rel">
                </div>
              </div>
              <div class="form_box wrapper">
                <label class="f_left form_box_text">Пароль<span class="red">*</span></label>
                <div class="w30 f_left">
                  <input type="password" class="mb6 p_rel">
                </div>
              </div>
              <div class="form_box wrapper">
                <label class="f_left form_box_text">Повторите пароль<span class="red">*</span></label>
                <div class="f_left w65 invalid">
                  <div class="w46 d_ib">
                    <input type="password" class="mb30 p_rel">
                  </div>
                  <span>Пароли не совпадают</span>
                </div>
              </div>
              <div class="form_box wrapper">
                <label class="f_left form_box_text">ФИО<span class="red">*</span></label>
                  <div class="w43 f_left">
                    <input type="text" class="mb6 p_rel" value="Василий Иванович Иванов" onblur="if(this.value=='') this.value='Василий Иванович Иванов'" onfocus="if(this.value =='Василий Иванович Иванов' ) this.value=''">
                  </div>
              </div>
              <div class="form_box wrapper">
                <label class="f_left form_box_text">E-mail<span class="red">*</span></label>
                <div class="invalid">
                  <div class="f_letf w43 d_ib">
                    <input type="text" class="mb6 p_rel">
                  </div>
                  <span>Ошибка</span>
                </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Телефон<span class="red">*</span></label>
                  <div class="w21 f_left">
                    <input type="tel" class="phone mb6 p_rel">
                  </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Факс<span class="red">*</span></label>
                  <div class="w21 f_left">
                    <input type="tel" class="phone mb6 p_rel">
                  </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Компания</label>
                  <div class="w43 f_left">
                    <input type="text" class="mb6 p_rel">
                  </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Должность</label>
                  <div class="w43 f_left">
                    <input type="text" class="mb6 p_rel">
                  </div>
              </div>
              <div class="form_box clearfix">
                <label class="f_left form_box_text">Город/Регион</label>
                <div class="f_left w30 custom_select main_select mb8">
                  <select class="mb6">
                    <option>Выберите город</option>
                    <option>Санкт-Петербург</option>
                    <option>Москва</option>
                    <option>Рязань</option>
                  </select>
                </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Сайт</label>
                  <div class="w43 f_left">
                    <input type="text" class="mb6 p_rel">
                  </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">ICQ</label>
                  <div class="w43 f_left">
                    <input type="text" class="mb6 p_rel">
                  </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Skype</label>
                  <div class="w43 f_left">
                    <input type="text" class="mb6 p_rel">
                  </div>
              </div>
              <div class="form_box wrapper">
                  <label class="f_left form_box_text">Код проверки</label>
                  <div class="clearfix">
                    <div class="w21 f_left">
                      <input type="text" class="mb20 p_rel">
                    </div>
                    <span class="f_left form_box_code al_center">
                      <img src="images/form_code.png" alt="code">
                    </span>
                    <button class="update_btn f_left"> Обновить</button>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <p>
                Для регистрации надо заполнить небольшую форму. Если Вы представляете компанию-поставщика
                электронных компонентов - заполните карточку компании на втором шаге.
            </p> 
            <p>
                Пожалуйста, заполните все поля, отмеченные <span class="red">*</span> звездочкой.<br> На Ваш e-mail будет выслана ссылка активации, кликнув по ней, вы получите доступ в личный кабинет.
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-5 col-lg-offset-2">
              <div class="form_box pb10">
                  <input type="checkbox" id="consent" class="d_ib" checked="">
                  <label class="form_box_lb" for="consent">Я согласен получать рассылку новостей</label>
              </div>
              <div class="form_box pb45">
                  <input type="checkbox" id="consent1" class="d_ib">
                  <label class="form_box_lb" for="consent1">
                    <a href="#" class="black_lk">C пользовательским соглашением </a> ознакомлен и согласен
                  </label>
                  <button class="red_btn mt20">Зарегистрироваться на сайте</button>
              </div>
          </div>
         
        </div>
      </div>
    </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>