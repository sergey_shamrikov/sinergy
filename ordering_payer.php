﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ordering payer</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="sh_box1">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <h2 class="title1 search_title">Оформление заказа</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="ordering_box">
      <div class="container-fluid">
          <ul class="ordering_delivery_list">
            <li class="ordering_list_item1"><a href="ordering_delivery.php" class="ordering_list_lk">Доставка</a></li>
            <li class="ordering_list_item2 current"><a href="ordering_payer.php" class="ordering_list_lk">Плательщик</a></li>
            <li class="ordering_list_item3"><a href="ordering_order.php" class="ordering_list_lk">Заказ</a></li>
            <li class="ordering_list_item3"><a href="ordering_payment.php" class="ordering_list_lk">Оплата</a></li>
          </ul>
      </div>
      <div class="bg_white">
        <div class="container-fluid">
          <form>
            <div class="row">
              <div class="col-lg-5 col-lg-offset-2">
                <div class="setting_user_box wrapper mb10">
                  <span class="d_block pb13">Укажите реквизиты плательщика для выставления счета или квитанции в Сбербанк</span>
                  <div class="f_left setting_user">
                    <input type="radio" id="form_user1" class="d_ib" name="user1" checked>
                    <label class="form_type_lb" for="form_user1">Юридическое лицо</label>
                  </div>
                  <div class="f_left setting_user">
                    <input type="radio" id="form_user2" class="d_ib" name="user1">
                    <label class="form_type_lb" for="form_user2">Предприниматель</label>
                  </div>
                  <div class="f_left setting_user">
                    <input type="radio" id="form_user3" class="d_ib" name="user1">
                    <label class="form_type_lb" for="form_user3">Физ. лицо</label>
                  </div>
                </div>
             </div>
            </div>
            <div class="row">
              <div class="col-lg-9">
                <div class="form_box wrapper">
                  <label class="f_left form_box_sub">Компания<span class="red">*</span></label>
                  <div class="w30 f_left">
                      <input type="text" class="mb8">
                  </div>
                </div>
                <div class="form_box wrapper">
                  <label class="f_left form_box_sub">Телефон<span class="red">*</span></label>
                  <div class="w15 f_left">
                    <input type="tel" class="phone mb8">
                  </div>
                </div>
                <div class="form_box wrapper">
                  <label class="f_left form_box_sub">Адрес юр.<span class="red">*</span></label>
                  <div class="f_left w70">
                    <input type="tel" class="phone mb8">
                  </div>
                </div>
                <div class="form_box wrapper">
                  <label class="f_left form_box_sub">ИНН<span class="red">*</span></label>
                  <div class="w30 f_left">
                    <input type="text" class="mb8">
                  </div>
                </div>
                <div class="form_box wrapper">
                  <label class="f_left form_box_sub">КПП<span class="red">*</span></label>
                  <div class="w30 f_left">
                    <input type="text" class="mb4">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-lg-offset-2">
                <div class="box_order_links">
                  <button class="red_btn">Продолжить оформление заказа</button>
                  <button class="black3_btn">Назад</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

<!--==============================footer=================================-->
 <?php include("main_blocks/footer.php") ?>
   
</body>
</html>