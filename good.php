﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>provider</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="sh_box">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Интегральные микросхемы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Аудио</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Предохранители, фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Карты и модули памяти</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Реле</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Трансформаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Конденсаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Датчики, сенсоры</a>
                  <ul class="sub_catalog_list">
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Аксессуары</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Модульные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Пленочные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Танталовые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                    </li>
                  </ul>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Дискретные полупроводники</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Разъемы, соединители</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <ul class="breadcrumbs clearfix">
                <li class="breadcrumbs_item d_in-block">
                  <a href="sinergia_providers.html" class="breadcrumbs_link">
                    <span class="breadcrumbs_current">Каталог</span>
                  </a>
                  /
                </li>
                <li class="breadcrumbs_item d_in-block">
                  <a href="sinergia_providers.html" class="breadcrumbs_link">
                    <span class="breadcrumbs_current">Semiconductor</span>
                  </a>
                  /
                </li>
                <li class="breadcrumbs_item d_in-block">
                  <a href="sinergia_providers.html" class="breadcrumbs_link">
                    <span class="breadcrumbs_current">Diodes</span>
                  </a>
                  /
                </li>
                <li class="breadcrumbs_item d_in-block">
                  <span class="breadcrumbs_current">Bridge Recrifier</span>
                </li>
              </ul>
          </div>
        </div>
        <div class="row mb10">
          <div class="col-lg-2">
            <div class="provider-logo2 al_right">
                <img src="images/sinergy_good_img1.png" alt="">
            </div>
          </div>
          <div class="col-lg-6">
            <h3 class="title1 prov_titel_item">MAX232ACPE+</h3>
            <p class="mb10">
              Производитель: Maxim Integrated
            </p>
            <p>
              Описание: ИС, интерфейс RS-232 5V MultiCh RS-232 Driver/Receiver
            </p>
            <div class="pb20">
              <a href="javascript:;" class="product_lk1">Поделиться</a>
              <a href="javascript:;" class="product_lk2">Спросить о товаре</a>
              <a href="javascript:;" class="product_lk3">Сообщить об ошибке</a>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="box_good1">
              <div class="pb13 box_good1_item1 al_left">
                <a href="javascript:;" class="product_lk4">Версия для печати</a>
              </div>
              <p><img class="box_good1_img" src="images/fairchild.png" alt=""></p>
              <div class="pb8">
                <button class="white_btn">Добавить к сравнению</button>
              </div>
              <div class="pb20">
                <button class="pdf_type_btn white_btn">Техническое описание</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <h3 class="title1 al_center pt35">Наличие и цены</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <table class="order_tb sinergia_good_table mt20">
              <tbody>
                <tr>
                  <th class="col3">Наименование</th>
                  <th class="col3">Производитель</th>
                  <th class="col3">Цена</th>
                  <th class="col3">Поставщик</th>
                  <th class="col3">Артикул</th>
                  <th class="col3">Склад</th>
                  <th class="col4"><span class="tab_aft_dwn">Срок поставки</span></th>
                  <th class="col3"></th>
                </tr>
                <tr>
                  <td class="bold">MAX232ACPE+</td>
                  <td>Maxim Integrated</td>
                  <td> 
                    <span class="d_block">1шт.:320,60 <span class="rub">a</span></span>
                    <span class="d_block">10шт.:318,14 <span class="rub">a</span></span>
                    <span class="d_block">50шт.:306,78 <span class="rub">a</span></span>
                  </td>
                  <td>Farnell</td>
                  <td>A1303-ND</td>
                  <td> 
                    <span class="d_block">90 шт.</span>
                    <span class="d_block">Мин. заказ:10шт.</span>
                    <span class="d_block">Кратность:10шт.</span>
                  </td>
                  <td>2 недели,<span class="delivery_time"> 03.02.15</span></td>
                  <td>
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <a href="#" class="add_btn">Добавлено <span class="counter_added">30шт</span></a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="bold">MAX232ACPE+</td>
                  <td>Maxim Integrated</td>
                  <td> 
                    <span class="d_block">1шт.:320,60 <span class="rub">a</span></span>
                    <span class="d_block">10шт.:318,14 <span class="rub">a</span></span>
                    <span class="d_block">50шт.:306,78 <span class="rub">a</span></span>
                  </td>
                  <td>Digikey</td>
                  <td>A1303-ND</td>
                  <td> 
                    <span class="d_block">50 шт.</span>
                    <span class="d_block">Мин. заказ:50шт.</span>
                    <span class="d_block">Кратность:50шт.</span>
                  </td>
                  <td>2 недели,<span class="delivery_time"> 03.02.15</span></td>
                  <td>
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <a href="#" class="add_btn">Добавлено <span class="counter_added">30шт</span></a>
                    </div>
                  </td>
                </tr>
                <tr class="gray2">
                  <td class="bold">MAX232ACPE+</td>
                  <td>Maxim Integrated</td>
                  <td> 
                    <span class="d_block">1шт.:320,60 <span class="rub">a</span></span>
                    <span class="d_block">10шт.:318,14 <span class="rub">a</span></span>
                    <span class="d_block">50шт.:306,78 <span class="rub">a</span></span>
                  </td>
                  <td>Рога и копыта</td>
                  <td>A1303-ND</td>
                  <td> 
                    <span class="d_block">Нет в наличии</span>
                  </td>
                  <td>2 недели,<span class="delivery_time"> 03.02.15</span></td>
                  <td>
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="active red_btn">В корзину</button>
                      <a href="#" class="add_btn">Добавлено <span class="counter_added">30шт</span></a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>