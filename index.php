﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Home</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
   <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body class="main_page">
  
  <!--==============================header=================================-->
   <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 maxheight">
            <div class="catalog_box catalogMain">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Интегральные микросхемы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Аудио</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Предохранители, фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Карты и модули памяти</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Реле</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Трансформаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Конденсаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Датчики, сенсоры</a>
                  <ul class="sub_catalog_list">
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Аксессуары</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Модульные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Пленочные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Танталовые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                    </li>
                  </ul>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Дискретные полупроводники</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Разъемы, соединители</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-10 maxheight">
            <div class="p_rel mt-96 mb25">
              <div class="main_slider_bg p_abs"></div>
              <div id="main_slider" class="owl-carousel">
                <div class="slider_item">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="main_slider_img al_right">
                        <img src="images/main_slider_img.png" alt="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="main_slider_box">
                        <h5 class="main_slider_title1 p_rel"><i class="p_abs"></i>Raspberry Pi</h5>
                        <h6 class="main_slider_title2">Model B 512MB</h6>
                        <p class="main_slider_text mb10">
                          Компания Respberry Pi выпустила обновлённый одноплатный компьютер, позиционирующийся как дешёвое решение для начинающих разработчиков, теперь на борту 512 мегабайт оперативной памяти и другие плюшки...
                        </p>
                        <a href="#" class="button_link1">Посмотреть характеристики</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="slider_item">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="main_slider_img al_right">
                        <img src="images/main_slider_img.png" alt="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="main_slider_box">
                        <h5 class="main_slider_title1 p_rel"><i class="p_abs"></i>Raspberry Pi</h5>
                        <h6 class="main_slider_title2">Model B 512MB</h6>
                        <p class="main_slider_text mb10">
                          Компания Respberry Pi выпустила обновлённый одноплатный компьютер, позиционирующийся как дешёвое решение для начинающих разработчиков, теперь на борту 512 мегабайт оперативной памяти и другие плюшки...
                        </p>
                        <a href="#" class="button_link1">Посмотреть характеристики</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="slider_item">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="main_slider_img al_right">
                        <img src="images/main_slider_img.png" alt="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="main_slider_box">
                        <h5 class="main_slider_title1 p_rel"><i class="p_abs"></i>Raspberry Pi</h5>
                        <h6 class="main_slider_title2">Model B 512MB</h6>
                        <p class="main_slider_text mb10">
                          Компания Respberry Pi выпустила обновлённый одноплатный компьютер, позиционирующийся как дешёвое решение для начинающих разработчиков, теперь на борту 512 мегабайт оперативной памяти и другие плюшки...
                        </p>
                        <a href="#" class="button_link1">Посмотреть характеристики</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="mb40">
              <h3 class="title1 mb10">Лучшие электронные компоненты по выгодным ценам</h3>
              <p>
                Наш магазин работает на заказы от 1-ой штуки, но сумма заказа должна быть минимум 500 рублей. В нашем магазине вы просто указываете наименование и тут же получаете точное количество, точную цену для нужного вам количества, дату поставки в Москву. Сразу видите размер причитающейся Вам скидки! Вы самостоятельно выбираете метод доставки. После оформления заказа в режиме онлайн прямо на сайте получаете счет для безналичной оплаты. Счет так же возможно оплатить кредитными картами или электронными деньгами. 
              </p>
              <p>
                Наша система полностью автоматизирована. Заказ приходит к нам на склад - Вы сразу получаете уведомление по e-mail и SMS на Ваш мобильный! Действует гибкая система скидок на сумму заказа. Количество обслуживаемых складов постоянно растет, а значит, Вам становится удобнее закрывать свою потребность в электронных компонентах одним счетом. Автоматизация позволяет нам снижать цены и сокращать сроки доставки. Все просто, попробуйте раз и надеемся мы станем друзьями!
              </p>
            </div>
          </div>
        </div>
      </div>
      
      <!-- carousel -->
      <div class="bg_black pt35">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <h3 class="title2 al_center mb15">Последние новости</h3>
              <div class="news_slider owl-carousel">
                <div class="news_slider_item clearfix">
                  <div class="news_slider_data f_left">
                    <strong>18</strong>
                    <span>12.14</span>
                  </div>
                  <p class="news_slider_text">
                    Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки вы сможете создать нереальную...
                  </p>
                </div>
                <div class="news_slider_item clearfix">
                  <div class="news_slider_data f_left">
                    <strong>07</strong>
                    <span>12.14</span>
                  </div>
                  <p class="news_slider_text">
                    Данные катушки индуктивности имеют обмотку из плоского провода, окончания обмотки используются  как выводы. Рассчитаны на высокий...
                  </p>
                </div>
                <div class="news_slider_item clearfix">
                  <div class="news_slider_data f_left">
                    <strong>24</strong>
                    <span>10.14</span>
                  </div>
                  <p class="news_slider_text">
                    Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления...
                  </p>
                </div>
                <div class="news_slider_item clearfix">
                  <div class="news_slider_data f_left">
                    <strong>09</strong>
                    <span>10.14</span>
                  </div>
                  <p class="news_slider_text">
                    Двухлинейные гибридные сетевые фильтры общего назначения серии PLY10 производства Murata предназначены для подавления...
                  </p>
                </div>
                <div class="news_slider_item clearfix">
                  <div class="news_slider_data f_left">
                    <strong>18</strong>
                    <span>12.14</span>
                  </div>
                  <p class="news_slider_text">
                    Лазерная цветомузыка – это модное и успешное направление организаторской мысли. С помощью лазерной установки вы сможете создать нереальную...
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- carousel providers -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 pt40 pb32">
            <h3 class="title1 al_center mb15">Наши поставщики</h3>
            <div class="providers_slider owl-carousel">
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo1.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo2.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo3.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo4.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo5.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo1.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo2.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo3.jpg" alt=""></a>
              </div>
              <div class="providers_slider_item">
                <a href="#" class="d_block"><img src="images/providers_logo4.jpg" alt=""></a>
              </div>
            </div>
            <div class="al_center">
              <a href="#" class="white_btn">Посмотреть всех</a>
            </div>
          </div>
        </div>
      </div>
  </div>
  <!-- ============================= footer ================================ -->
  <?php include("main_blocks/footer.php") ?>

</body>
</html>