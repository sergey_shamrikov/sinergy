﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Private office</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="sh_box">
      <div class="container-fluid">
         <div class="row">
          <div class="col-lg-2 col-md-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Интегральные микросхемы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Аудио</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Предохранители, фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Фильтры</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Карты и модули памяти</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Реле</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Трансформаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Конденсаторы</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Датчики, сенсоры</a>
                  <ul class="sub_catalog_list">
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Аксессуары</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Модульные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Пленочные</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Танталовые</a>
                    </li>
                    <li class="sub_catalog_item">
                      <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                    </li>
                  </ul>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Дискретные полупроводники</a>
                </li>
                <li class="catalog_item">
                  <a href="#" class="catalog_link">Разъемы, соединители</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <h2 class="title1 catalog_user">Васин Сергей Васильевич: <span class="title_id">ID34313134</span></h2>
          </div>
          <div class="col-lg-4 al_right">
            <div class="d_ib">
              <div class="personal_meneger d_tb">
                <div class="d_tc"><img src="images/meneger.png" height="48" width="48" alt="picture"></div>
                <div class="d_tc pers_box_right al_left">
                  <span class="pers_men_name">Ваш личный менеджер: Василиса</span>
                  <span class="d_in-block pers_men_tel">Тел.: +7 812 923 25 29</span>
                  <span class="d_in-block pers_men_post">Почта:<a href="mailto:vasilisa@sinergy.ru">vasilisa@sinergy.ru</a></span>
                  <div class="al_just pers_men_inner drop_down_box">
                    <button class="white_btn">Заказать звонок</button>
                    <button class="white_btn drop_down_btn bdr5">Написать менеджеру</button>
                    <div class="drop_down drop_down_right">
                  <form class="dropdown_form p_rel">
                    <ul class="meneg_mess_main">
                      <li class="meneg_mess_item">
                        <h6>Василиса в 13:37</h6>
                        <p>
                          Добрый день! У вас возникли вопросы?
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Вы в 13:37</h6>
                        <p>
                          Мне нужна выписка для налоговой.
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Василиса в 13:38</h6>
                        <p>
                          Да конечно, оставьте свой адрес вашей эл.почты, в течение 20 минут я вышлю вам всё необходимое.
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Вы в 13:42</h6>
                        <p>
                          Большое спасибо, вот почта: vasja@pupkin.ru
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Василиса в 13:59</h6>
                        <p>
                          Отправила выписку на почту, пожалуйста, проверьте.
                        </p>
                      </li>
                      <li class="meneg_mess_item">
                        <h6>Вы в 13:42</h6>
                        <p>
                          Ещё раз спасибо, всё получил!
                        </p>
                      </li>
                    </ul>
                    <textarea class="mb6" onblur="if(this.value=='') this.value='Комментарий'" onfocus="if(this.value =='Комментарий' ) this.value=''">Сообщение</textarea>
                    <div class="al_right">
                      <button class="red_btn">Отправить</button>
                    </div>
                  </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
    <div id="tabs" class="history_tabs">
        <div class="container-fluid">
          <ul class="resp-tabs-list clearfix">
              <li class="resp-list_item1">Мои заказы</li>
              <li class="resp-list_item2">История платежей</li>
              <li class="resp-list_item3">Мои данные</li>
          </ul>
        </div>
        <div class="resp-tabs-container">
          <div>
            <div class="container-fluid">
              <div class="discount">
                <div class="discount_item1">
                    <span>Ваша скидка:3%</span>
                      <div class="op50">
                        <span>До 100 000<span class="d_ib">P<span class="d_block ruble"></span></span></span>
                      </div>
                </div>
                <div class="discount_item2">
                    <span>Ваша скидка:5%</span>
                    <div class="op50">
                      <span>Более 100 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                    </div>
                </div>
                <div class="discount_item3">
                    <span>Ваша скидка:7%</span>
                    <div class="op50">
                      <span>Более 200 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                    </div>
                </div>
                <div class="discount_item4">
                    <span>Ваша скидка:10%</span>
                    <div class="op50">
                      <span>Более 400 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                    </div>
                </div>
                <div class="discount_item5">
                    <span>Особые условия</span>
                      <div class="op50">
                        <span>Более 600 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                      </div>
                </div>
                <div class="skillbar clearfix " data-percent="5%">
                  <div class="skillbar-bar d_ib"></div>
                  <div class="skillbar-title d_ib">
                    <span class="counter_discount"> 74 000
                      <span class="d_ib">P<span class="d_block ruble1"></span></span>
                    </span> 
                  </div>
                  <div class="skill-bar-percent">5%</div>
                </div> 
                <span class="op50">Для получения скидки 5% осталось заказать товаров на сумму 26 000 рублей</span>
              </div>
            </div>
            <div class="bg_black">
              <div class="container-fluid">
                <div class="sorting p_rel">
                  <form>
                    <div class="sorting_box d_ib w15">
                      <input type="text" class="sorting_item" value="Номер заказа" onblur="if(this.value=='') this.value='Номер заказа'" onfocus="if(this.value =='Номер заказа' ) this.value=''">
                    </div>
                    <div class="sorting_box d_ib w15">
                      <input type="text" class="sorting_item" value="Дата заказа" onblur="if(this.value=='') this.value='Дата заказа'" onfocus="if(this.value =='Дата заказа' ) this.value=''">
                    </div>
                    <div class="w15 custom_select main_select mb8 d_ib sorting_box">
                      <select class="mb6">
                        <option>Статус оплаты</option>
                        <option>Оплачен</option>
                        <option>Ожидает оплаты</option>
                      </select>
                    </div>
                    <div class="w15 custom_select main_select mb8 d_ib sorting_box">
                      <select class="mb6">
                        <option>Статус заказа</option>
                        <option>На оформлении</option>
                        <option>Оформлен</option>
                        <option>Отправлен</option>
                        <option>Доставлен</option>
                      </select>
                    </div>
                    <div class="sorting_box d_ib w15">
                      <input type="text" class="sorting_item" value="Номер заказа (внутр.)" onblur="if(this.value=='') this.value='Номер заказа(внутр.)'" onfocus="if(this.value =='Номер заказа (внутр.)' ) this.value=''">
                    </div>
                    <div class="sorting_box d_ib w15">
                      <input type="text" class="sorting_item" value="Дата заказа (внутр.)" onblur="if(this.value=='') this.value='Дата заказа(внутр.)'" onfocus="if(this.value =='Дата заказа (внутр.)' ) this.value=''">
                    </div>
                    <button class="sorting_btn p_abs">Сброс</button>
                  </form>
                </div>
              </div>
            </div>
            <div class="bg_white">
              <table class="order_tb mt30">
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <th class="col3">№ Заказа</th>
                          <th class="col3">Дата заказа</th>
                          <th class="col3">Статус заказа</th>
                          <th class="col3">Сумма</th>
                          <th class="col4">Статус оплаты</th>
                          <th class="col4">Номер заказа(внутр.)</th>
                          <th class="col4">Дата заказа(внутр.)</th>
                          <th class="col5">Доставка</th>
                          <th class="col6"></th>
                        </tr>
                        <tr class="">
                          <td class="bold">
                            <a href="javascript:;" class="orders_more_btn">23132</a>
                          </td>
                          <td>21.11.14 19:29</td>
                          <td>Оформление</td>
                          <td>140 320,60<span class="rub">a</span></td>
                          <td>Ожидает оплаты</td>
                          <td>УТВП 000347</td>
                          <td>21.11.14 19:29</td>
                          <td>
                            <span class="d_block">Курьерская служба «Деловые линии»</span>
                            <span class="d_block">Ожидаемая дата доставки 05.01.15</span>
                            <span class="d_block">Трек-номер: 1233 4222 2112</span>
                            </td>
                          <td class="col6"><div class="drop_down_box">
                        <button class="red_btn ml30 drop_down_btn">Оплатить</button>
                        <div class="drop_down drop_down_right">
                          <form class="dropdown_form">
                            <ul class="cash">
                              <li class="cash_item">
                                <a href="#" class="cash_link">Безналичным переводом</a>
                                <p class="cash_day">1-3 рабочих дня</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                                <p class="cash_day">Мгновенно и без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                                <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через интернет-банк</a>
                                <p class="cash_day">Без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">В салонах мобильной связи</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через платежные терминалы</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                            </ul>
                          </form>
                        </div>
                      </div></td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="9">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="bold col3">
                            <a href="javascript:;" class="orders_more_btn">24074</a></td>
                          <td class="col3">07.12.14 07:30</td>
                          <td class="col3">Отправлен</td>
                          <td class="col3">275 003,30<span class="rub">a</span></td>
                          <td class="col4">Оплачен</td>
                          <td class="col4">УТВП 000301</td>
                          <td class="col4">07.12.14 07:30</td>
                          <td class="col5">
                            <span class="d_block">Курьерская служба «Деловые линии»</span>
                            <span class="d_block">Трек-номер: 1233 4222 2112</span>
                          </td>
                          <td class="col6"><div class="drop_down_box">
                        <button class="red_btn ml30 drop_down_btn">Оплатить</button>
                        <div class="drop_down drop_down_right">
                          <form class="dropdown_form">
                            <ul class="cash">
                              <li class="cash_item">
                                <a href="#" class="cash_link">Безналичным переводом</a>
                                <p class="cash_day">1-3 рабочих дня</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                                <p class="cash_day">Мгновенно и без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                                <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через интернет-банк</a>
                                <p class="cash_day">Без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">В салонах мобильной связи</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через платежные терминалы</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                            </ul>
                          </form>
                        </div>
                      </div></td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="9">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="bold col3">
                            <a href="javascript:;" class="orders_more_btn">35012</a></td>
                          <td class="col3">26.12.14 09:41</td>
                          <td class="col3">Доставлен</td>
                          <td class="col3">18 768,59<span class="rub">a</span></td>
                          <td class="col4">Оплачен</td>
                          <td class="col4">УТВП 000292</td>
                          <td class="col4">26.12.14 09:41</td>
                          <td class="col5">
                            <span class="d_block">Курьерская служба «Деловые линии»</span>
                            <span class="d_block">Трек-номер: 1233 4222 2112</span>
                          </td>
                          <td class="col6"><div class="drop_down_box">
                        <button class="red_btn ml30 drop_down_btn">Оплатить</button>
                        <div class="drop_down drop_down_right">
                          <form class="dropdown_form">
                            <ul class="cash">
                              <li class="cash_item">
                                <a href="#" class="cash_link">Безналичным переводом</a>
                                <p class="cash_day">1-3 рабочих дня</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                                <p class="cash_day">Мгновенно и без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                                <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через интернет-банк</a>
                                <p class="cash_day">Без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">В салонах мобильной связи</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через платежные терминалы</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                            </ul>
                          </form>
                        </div>
                      </div></td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="9">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col3 bold">
                            <a href="javascript:;" class="orders_more_btn">23132</a></td>
                          <td class="col3">21.11.14 19:29</td>
                          <td class="col3">Оформление</td>
                          <td class="col3">140 320,60<span class="rub">a</span></td>
                          <td class="col4">Ожидает оплаты</td>
                          <td class="col4">УТВП 000347</td>
                          <td class="col4">21.11.14 19:29</td>
                          <td class="col5">
                            <span class="d_block">Курьерская служба «Деловые линии»</span>
                            <span class="d_block">Ожидаемая дата доставки 05.01.15</span>
                            <span class="d_block">Трек-номер: 1233 4222 2112</span>
                          </td>
                          <td class="col6"><div class="drop_down_box">
                        <button class="red_btn ml30 drop_down_btn">Оплатить</button>
                        <div class="drop_down drop_down_right">
                          <form class="dropdown_form">
                            <ul class="cash">
                              <li class="cash_item">
                                <a href="#" class="cash_link">Безналичным переводом</a>
                                <p class="cash_day">1-3 рабочих дня</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                                <p class="cash_day">Мгновенно и без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                                <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через интернет-банк</a>
                                <p class="cash_day">Без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">В салонах мобильной связи</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через платежные терминалы</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                            </ul>
                          </form>
                        </div>
                      </div></td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="9">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col3 bold">
                            <a href="javascript:;" class="orders_more_btn">24074</a>
                          </td>
                          <td class="col3">07.12.14 07:30</td>
                          <td class="col3">Отправлен</td>
                          <td class="col3">275 003,30<span class="rub">a</span></td>
                          <td class="col4">Оплачен</td>
                          <td class="col4">УТВП 000301</td>
                          <td class="col4">07.12.14 07:30</td>
                          <td class="col5">
                            <span class="d_block">Курьерская служба «Деловые линии»</span>
                            <span class="d_block">Трек-номер: 1233 4222 2112</span>
                          </td>
                          <td class="col6"><div class="drop_down_box">
                        <button class="red_btn ml30 drop_down_btn">Оплатить</button>
                        <div class="drop_down drop_down_right">
                          <form class="dropdown_form">
                            <ul class="cash">
                              <li class="cash_item">
                                <a href="#" class="cash_link">Безналичным переводом</a>
                                <p class="cash_day">1-3 рабочих дня</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                                <p class="cash_day">Мгновенно и без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                                <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через интернет-банк</a>
                                <p class="cash_day">Без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">В салонах мобильной связи</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через платежные терминалы</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                            </ul>
                          </form>
                        </div>
                      </div></td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="9">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col3 bold">
                            <a href="javascript:;" class="orders_more_btn">35012</a>
                          </td>
                          <td class="col3">26.12.14 09:41</td>
                          <td class="col3">Доставлен</td>
                          <td class="col3">18 768,59<span class="rub">a</span></td>
                          <td class="col4">Оплачен</td>
                          <td class="col4">УТВП 000292</td>
                          <td class="col4">26.12.14 09:41</td>
                          <td class="col5">
                            <span class="d_block">Курьерская служба «Деловые линии»</span>
                            <span class="d_block">Трек-номер: 1233 4222 2112</span>
                          </td>
                          <td class="col6"><div class="drop_down_box">
                        <button class="red_btn ml30 drop_down_btn">Оплатить</button>
                        <div class="drop_down drop_down_right">
                          <form class="dropdown_form">
                            <ul class="cash">
                              <li class="cash_item">
                                <a href="#" class="cash_link">Безналичным переводом</a>
                                <p class="cash_day">1-3 рабочих дня</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Картой Visa или MasterCard</a>
                                <p class="cash_day">Мгновенно и без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Квитанция в Сбербанк</a>
                                <p class="cash_day">5-10 рабочих дней, комиссия около 3%</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через интернет-банк</a>
                                <p class="cash_day">Без комиссий</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">В салонах мобильной связи</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                              <li class="cash_item">
                                <a href="#" class="cash_link">Через платежные терминалы</a>
                                <p class="cash_day">Может взиматься комиссия</p>
                              </li>
                            </ul>
                          </form>
                        </div>
                      </div></td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="9">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
              <div class="col-lg-4 col-lg-offset-8">
                  <button class="white_btn">Экспортировать все в Excel</button>
                  <button class="white_btn">Распечатать заказы</button>
              </div>
              <div class="al_center pt35 pb20">
                <a href="javascript:;" class="white_bd_lk">Показать еще</a>
              </div>
            </div>
          </div>
          <div>
            <div class="container-fluid">
              <div class="discount">
                <div class="discount_item1">
                    <span>Ваша скидка:3%</span>
                      <div class="op50">
                        <span>До 100 000<span class="d_ib">P<span class="d_block ruble"></span></span></span>
                      </div>
                </div>
                <div class="discount_item2">
                    <span>Cкидка:5%</span>
                    <div class="op50">
                      <span>Более 100 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                    </div>
                </div>
                <div class="discount_item3">
                    <span>Cкидка:7%</span>
                    <div class="op50">
                      <span>Более 200 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                    </div>
                </div>
                <div class="discount_item4">
                    <span>Cкидка:10%</span>
                    <div class="op50">
                      <span>Более 400 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                    </div>
                </div>
                <div class="discount_item5">
                    <span>Особые условия</span>
                      <div class="op50">
                        <span>Более 600 000 <span class="d_ib">P<span class="d_block ruble"></span></span></span>
                      </div>
                </div>
                <div class="skillbar clearfix " data-percent="5%">
                  <div class="skillbar-bar d_ib"></div>
                  <div class="skillbar-title d_ib">
                    <span class="counter_discount"> 74 000
                      <span class="d_ib">P<span class="d_block ruble1"></span></span>
                    </span> 
                  </div>
                  <div class="skill-bar-percent">5%</div>
                </div> 
                <span class="op50">Для получения скидки 5% осталось заказать товаров на сумму 26 000 рублей</span>
              </div>
            </div>
            <div class="bg_black">
                <div class="container-fluid">
                  <div class="sorting p_rel">
                    <form>
                      <div class="sorting_box d_ib w15">
                        <input type="text" class="sorting_item" value="Номер платежа" onblur="if(this.value=='') this.value='Номер платежа'" onfocus="if(this.value =='Номер платежа' ) this.value=''">
                      </div>
                      <div class="sorting_box d_ib w15">
                        <input type="text" class="sorting_item" value="Номер заказа" onblur="if(this.value=='') this.value='Номер заказа'" onfocus="if(this.value =='Номер заказа' ) this.value=''">
                      </div>
                      <div class="sorting_box d_ib w15">
                        <input type="text" class="sorting_item" value="Дата платежа" onblur="if(this.value=='') this.value='Дата платежа'" onfocus="if(this.value =='Дата платежа' ) this.value=''">
                      </div>
                      <div class="sorting_box d_ib w15">
                        <input type="text" class="sorting_item" value="Сумма платежа" onblur="if(this.value=='') this.value='Сумма платежа'" onfocus="if(this.value =='Сумма платежа' ) this.value=''">
                      </div>
                      <div class="w15 custom_select main_select mb8 d_ib">
                        <select class="mb6">
                          <option>Способ оплаты</option>
                          <option>Квитанцией в Сбербанке</option>
                          <option>Через терминал Qiwi</option>
                          <option>Картой MasterCard</option>
                        </select>
                      </div>
                      <button class="sorting_btn p_abs">Сброс</button>
                    </form>
                  </div>
                  
                </div>
            </div>
            <div class="bg_white">
               <table class="order_tb mt30">
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <th class="col1">№ п/п</th>
                          <th class="col1">№ заказа</th>
                          <th class="col1">Дата платежа</th>
                          <th class="col1">Сумма</th>
                          <th class="col2">Способ оплаты</th>
                        </tr>
                        <tr>
                          <td class="col1 bold">
                            <a href="javascript:;" class="orders_more_btn">23132</a>
                          </td>
                          <td class="col1"><a href="javascript:;" class="black_lk">3132</a></td>
                          <td class="col1">21.11.14 19:29</td>
                          <td class="col1">140 320,60
                            <span class="d_ib rub">a</span>
                          </td>
                          <td class="col2">Через терминал Qiwi</td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="5">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col1 bold">
                            <a href="javascript:;" class="orders_more_btn">24074</a>
                          </td>
                          <td class="col1"><a href="javascript:;" class="black_lk">24074</a></td>
                          <td class="col1">07.12.14 07:30</td>
                          <td class="col1">275 003,30
                            <span class="d_ib rub">a</span>
                          </td>
                          <td  class="col2">Квитанцией в Сбербанке</td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="5">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col1 bold">
                            <a href="javascript:;" class="orders_more_btn">35012</a>
                          </td>
                          <td class="col1"><a href="javascript:;" class="black_lk">35012</a></td>
                          <td class="col1">26.12.14 09:41</td>
                          <td class="col1">18 768,59
                            <span class="d_ib rub">a</span>
                          </td>
                          <td class="col2">Картой MasterCard — 5469 **** **** 8848</td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="5">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col1 bold">
                            <a href="javascript:;" class="orders_more_btn">23132</a>
                          </td>
                          <td class="col1"><a href="javascript:;" class="black_lk">3132</a></td>
                          <td class="col1">21.11.14 19:29</td>
                          <td class="col1">140 320,60
                            <span class="d_ib rub">a</span>
                          </td>
                          <td class="col2">Через терминал Qiwi</td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="5">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col1 bold">
                            <a href="javascript:;" class="orders_more_btn">24074</a>
                          </td>
                          <td class="col1"><a href="javascript:;" class="black_lk">24074</a></td>
                          <td class="col1">07.12.14 07:30</td>
                          <td class="col1">275 003,30
                            <span class="d_ib rub">a</span>
                          </td>
                          <td  class="col2">Квитанцией в Сбербанке</td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="5">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="p_rel order_overlay">
                    <div class="container-fluid">
                      <table class="order_tb mt30">
                        <tr>
                          <td class="col1 bold">
                            <a href="javascript:;" class="orders_more_btn">35012</a>
                          </td>
                          <td class="col1"><a href="javascript:;" class="black_lk">35012</a></td>
                          <td class="col1">26.12.14 09:41</td>
                          <td class="col1">18 768,59
                            <span class="d_ib rub">a</span>
                          </td>
                          <td class="col2">Картой MasterCard — 5469 **** **** 8848</td>
                        </tr>
                        <tr class="orders_row_more">
                          <td colspan="5">
                            <div class="orders_more_box">
                              <table class="orders_office_more p_rel">
                                <tr>
                                  <th class="col7">№</th>
                                  <th class="col3">Наименование</th>
                                  <th class="col3">Производитель</th>
                                  <th class="col8">Описание</th>
                                  <th class="col9">Ко-во</th>
                                  <th class="col9">Цена</th>
                                  <th class="col9">Сумма</th>
                                  <th class="col3">Статус</th>
                                  <th class="col3">Поставщик</th>
                                  <th class="col6">Артикул</th>
                                  <th class="col3">Срок поставки</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td class="bold">MAX232ACPE+</td>
                                  <td>Maxim Integrated</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Maxim Integrated</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Texas Instruments</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Texas Instruments</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td class="bold">ADM232AANZ</td>
                                  <td>Analog device</td>
                                  <td>
                                    <span class="d_block">Standard Circular Connector</span>
                                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                                  </td>
                                  <td>3968 шт.</td>
                                  <td>320,60<span class="rub">a</span></td>
                                  <td>24320,60<span class="rub">a</span></td>
                                  <td>
                                    <span class="d_block">Отгружено:<span class="d_ib counter_shipping">100</span></span>
                                    <span class="d_block">На складе:<span class="d_ib counter_store">231</span></span>
                                    <span class="d_block">Доставляется:<span class="d_ib counter_delivered">210</span></span>
                                    <a href="#" class="d_ib link_ord_table">Подробнее</a>
                                  </td>
                                  <td>Analog device</td>
                                  <td>A1303-ND</td>
                                  <td>4 недели,<span class="delivery_time"> 03.02.15</span></td>
                                </tr>
                              </table>
                              <div class="row">
                                <div class="col-lg-5 col-lg-offset-7 orders_more_links al_right">
                                  <button class="white_btn">Экспортировать все в Excel</button>
                                  <button class="white_btn">Распечатать</button>
                                  <button class="white_btn">Добавить комментарий</button>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            <div class="row">
              <div class="col-lg-4 col-lg-offset-8">
                <button class="white_btn">Экспортировать в Excel</button>
                <button class="white_btn">Распечатать</button>
              </div>
            </div>
            <div class="al_center pt28 pb45">
              <a href="javascript:;" class="white_bd_lk">Показать еще</a>
            </div>
          </div>
          </div>
          <div>
            <div class="container-fluid">
              <div class="row">
                <div class="col-alex-4 col-lg-6 col-lg-offset-2">
                  <span>Редактирование профиля пользователя</span>
                  <div class="setting_user_box">
                    <form id="setting_user" class="wrapper">
                      <div class="f_left setting_user">
                        <input type="radio" id="form_user1" class="d_ib" name="user1" checked>
                        <label class="form_type_lb" for="form_user1">Юридическое лицо</label>
                      </div>
                      <div class="f_left setting_user">
                        <input type="radio" id="form_user2" class="d_ib" name="user1" checked>
                        <label class="form_type_lb" for="form_user2">Предприниматель</label>
                      </div>
                      <div class="f_left setting_user">
                        <input type="radio" id="form_user3" class="d_ib" name="user1" checked>
                        <label class="form_type_lb" for="form_user3">Физ. лицо</label>
                      </div>
                  </form>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-8 col-md-8">
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">ФИО<span class="red">*</span></label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel" value="Василий Иванович Иванов" onblur="if(this.value=='') this.value='Василий Иванович Иванов'" onfocus="if(this.value =='Василий Иванович Иванов' ) this.value=''">
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">E-mail<span class="red">*</span></label>
                    <div class="w70 f_left">
                      <div class="w30 d_ib">
                        <input type="text" class="mb6 p_rel" value="vasja@pupkin.ru" onblur="if(this.value=='') this.value='vasja@pupkin.ru'" onfocus="if(this.value =='vasja@pupkin.ru' ) this.value=''">
                      </div>
                      <span class="d_ib let1 form_box_subscrib gray3">
                      При смене e-mail будет автоматически отправлена ссылка для подтверждении
                    </span>
                    </div>
                 
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">Телефон</label>
                    <div class="w21 f_left">
                      <input type="tel" class="phone mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">Факс</label>
                    <div class="w21 f_left">
                      <input type="tel" class="phone mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">Компания</label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">Должность</label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">Сайт</label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box clearfix">
                    <label class="f_left form_box_text">Город/Регион</label>
                    <div class="f_left w23 custom_select main_select mb8">
                      <select class="mb6">
                        <option>Выберите город</option>
                        <option>Санкт-Петербург</option>
                        <option>Москва</option>
                        <option>Рязань</option>
                      </select>
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">ICQ</label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box mb20 wrapper">
                    <label class="f_left form_box_text">Skype</label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel">
                    </div>
                  </div>
                  <div class="form_box wrapper">
                    <label class="f_left form_box_text">Новый пароль</label>
                    <div class="w30 f_left">
                      <input type="text" class="mb6 p_rel">
                    </div>
                    <span class="f_left d_ib let1 form_box_subscrib gray3">Если пароль не требуется менять - оставьте поле пусты</span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-5 col-lg-offset-2">
                  <div class="form_box pb20">
                    <input type="checkbox" id="consent" class="d_ib mb20" checked>
                    <label class="form_box_lb mb20" for="consent">Я согласен получать рассылку новостей</label>
                    <div>
                      <button class="red_btn">Сохранить изменения</button>
                      <button class="black3_btn">Отменить изменения</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>