﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Payment_histopy</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
    <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-lg-2 p_rel">
              <div class="catalog_box catalog_hidden">
                <h5 class="catalog_title">Каталог</h5>
                <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-8">
              <h2 class="title1 catalog_user">В корзине 5 товаров</h2>
            </div>
          </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <table class="card_table_dilya sinergia_order_table">
                <tbody>
                  <tr class="al_left">
                    <th class="col3"></th>
                    <th class="col3">№</th>
                    <th class="col3"><span class="tab_aft_dwn">Наименование</span></th>
                    <th class="col3">Производитель</th>
                    <th class="col3">Описание</th>
                    <th class="col3">Поставщик</th>
                    <th class="col3">Артикул</th>
                    <th class="col3">Склад</th>
                    <th class="col3">Срок,поставки</th>
                    <th class="col3">Цена</th>
                    <th class="col3">Кол-во</th>
                    <th class="col3">Сумма</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td> 
                      <input type="checkbox" id="card1" class="d_ib">
                      <label class="form_box_lb" for="card1"></label>
                    </td>
                    <td>1</td>
                    <td class="bold">MAX232ACPE+</td>
                    <td>Maxim Integrated</td>
                    <td>
                      <span class="d_block">Standard Circular Connector </span>
                      <span class="d_block"> RECEPTACLE 9 PIN shell size 13</span>
                        <a class="pdf_btn d_block" href="#">Datasheet</a>
                    </td>
                    <td>Maxim Integrated</td>
                    <td>A1303-ND</td>
                    <td>
                      <span class="d_block">3968 шт.</span>
                      <span class="d_block">Мин.заказ:1 шт.</span>
                      <span class="d_block">Кратность: 1 шт.</span>
                    </td>
                    <td>4 недели,03.02.15</td>
                    <td>
                      <span class="d_block gray2">1 шт:320.60<span class="rub">a</span></span>
                      <span class="d_block gray2">10 шт:318,14<span class="rub">a</span></span>
                      <span class="d_block">50 шт:320.60<span class="rub">a</span></span>
                    </td>
                    <td>
                      <div class="d_ib w60p">
                        <input type="text" class="d_ib">
                      </div>
                    </td>
                    <td>3245,48<span class="rub">a</span></td>
                    <td>
                      <a href="#" class="close_table delete_btn"></a>
                    </td>
                  </tr>
                  <tr>
                    <td> 
                      <input type="checkbox" id="card2" class="d_ib">
                      <label class="form_box_lb" for="card2"></label>
                    </td>
                    <td>2</td>
                    <td class="bold">ADM232AANZ</td>
                    <td>Texas Instruments</td>
                    <td>
                      <span class="d_block">Standard Circular Connector </span>
                      <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                        <a class="pdf_btn d_block" href="#">Datasheet</a>
                    </td>
                    <td>Texas Instruments</td>
                    <td>A1303-ND</td>
                    <td>
                      <span class="d_block">3968 шт.</span>
                      <span class="d_block">Мин.заказ:1 шт.</span>
                      <span class="d_block">Кратность: 1 шт.</span>
                    </td>
                    <td>4 недели,03.02.15</td>
                    <td>
                      <span class="d_block gray2">1 шт:320.60<span class="rub">a</span></span>
                      <span class="d_block">10 шт:318,14<span class="rub">a</span></span>
                      <span class="d_block gray2">50 шт:320.60<span class="rub">a</span></span>
                    </td>
                    <td>
                      <div class="d_ib w60p">
                        <input type="text" class="d_ib">
                      </div>
                    </td>
                    <td>15 068,20<span class="rub">a</span></td>
                    <td>
                      <a href="#" class="close_table delete_btn"></a>
                    </td>
                  </tr>
                   <tr>
                    <td> 
                      <input type="checkbox" id="card3" class="d_ib">
                      <label class="form_box_lb" for="card3"></label>
                    </td>
                    <td>3</td>
                    <td class="bold">ADM232AANZ</td>
                    <td>Analog device</td>
                    <td>
                      <span class="d_block">Standard Circular Connector </span>
                      <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                        <a class="pdf_btn d_block" href="#">Datasheet</a>
                    </td>
                    <td>Analog device</td>
                    <td>A1303-ND</td>
                    <td>
                      <span class="d_block">3968 шт.</span>
                      <span class="d_block">Мин.заказ:1 шт.</span>
                      <span class="d_block">Кратность: 1 шт.</span>
                    </td>
                    <td>4 недели,03.02.15</td>
                    <td>
                      <span class="d_block gray2">1 шт:320.60<span class="rub">a</span></span>
                      <span class="d_block">10 шт:318,14<span class="rub">a</span></span>
                      <span class="d_block gray2">50 шт:320.60<span class="rub">a</span></span>
                    </td>
                    <td>
                      <div class="d_ib w60p">
                        <input type="text" class="d_ib">
                      </div>
                    </td>
                    <td>14226,12<span class="rub">a</span></td>
                    <td>
                      <a href="#" class="close_table delete_btn"></a>
                    </td>
                  </tr>
                  <tr>
                    <td> 
                      <input type="checkbox" id="card4" class="d_ib">
                      <label class="form_box_lb" for="card4"></label>
                    </td>
                    <td>4</td>
                    <td class="bold">ADM232AANZ</td>
                    <td>Texas Instruments</td>
                    <td>
                      <span class="d_block">Standard Circular Connector </span>
                      <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                        <a class="pdf_btn d_block" href="#">Datasheet</a>
                    </td>
                    <td>Texas Instruments</td>
                    <td>A1303-ND</td>
                    <td>
                      <span class="d_block">3968 шт.</span>
                      <span class="d_block">Мин.заказ:1 шт.</span>
                      <span class="d_block">Кратность: 1 шт.</span>
                    </td>
                    <td>4 недели,03.02.15</td>
                    <td>
                      <span class="d_block gray2">1 шт:320.60<span class="rub">a</span></span>
                      <span class="d_block gray2">10 шт:318,14<span class="rub">a</span></span>
                      <span class="d_block">50 шт:320.60<span class="rub">a</span></span>
                    </td>
                    <td>
                      <div class="d_ib w60p">
                        <input type="text" class="d_ib">
                      </div>
                    </td>
                    <td>3245,48<span class="rub">a</span></td>
                    <td>
                      <a href="#" class="close_table delete_btn"></a>
                    </td>
                  </tr>
                  <tr>
                    <td> 
                      <input type="checkbox" id="card5" class="d_ib" >
                      <label class="form_box_lb" for="card5"></label>
                    </td>
                    <td>5</td>
                    <td class="bold">ADM232AANZ</td>
                    <td>Analog device</td>
                    <td>
                      <span class="d_block">Standard Circular Connector </span>
                      <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                        <a class="pdf_btn d_block" href="#">Datasheet</a>
                    </td>
                    <td>Analog device</td>
                    <td>A1303-ND</td>
                    <td>
                      <span class="d_block">3968 шт.</span>
                      <span class="d_block">Мин.заказ:1 шт.</span>
                      <span class="d_block">Кратность: 1 шт.</span>
                    </td>
                    <td>4 недели,03.02.15</td>
                    <td>
                      <span class="d_block gray2">1 шт:320.60<span class="rub">a</span></span>
                      <span class="d_block">10 шт:318,14<span class="rub">a</span></span>
                      <span class="d_block gray2">50 шт:320.60 <span class="rub">a</span></span>
                    </td>
                    <td>
                      <div class="d_ib w60p">
                        <input type="text" class="d_ib">
                      </div>
                    </td>
                    <td>15 068,20<span class="rub">a</span></td>
                    <td>
                      <a href="#" class="close_table delete_btn"></a>
                    </td>
                  </tr>
                </tbody> 
            </table>
            <div class="al_right clearfix mr30_1170">
                  <p class="bold title_fs_blec">Общая стоимость:50 853,48<span class="rub">a</span></p>
                  <p class="bold title_fs"> 3%:-1525,60<span class="rub">a</span></p>
                  <p class="bold title4">Итого без учета доставки: 49 327,88<span class="rub">a</span></p>
            </div>
            <div class="button_under_tab clearfix">
              <div class="f_left left_col_bt">
                <button class="white_bd_lk_2">Удалить выбранные</button>
              </div>
              <div class="f_right right_col_bt">
                <div class="butn_box">
                  <button class="print_but white_btn1">Распечатать</button>
                </div>
                <div class="butn_box">
                  <button class="white_btn1">Пересчитать</button>
                </div>
                <div class="drop_down_box butn_box">
                  <button class="red_btn drop_down_btn">Оформить заказ</button>
                  <div class="drop_up drop_down_right clearfix">
                    <form class="dropdown_form form_reg">
                    <div class="box_boton_input">
                      <p class="bold">Войдит</p>
                       <div>
                          <input type="text" class="mb6 p_rel" value="Эл.адрес" onblur="if(this.value=='') this.value='Эл.адрес'" onfocus="if(this.value =='Эл.адрес' ) this.value=''">
                        </div>
                        <div>
                          <input type="password" class="mb6 p_rel" placeholder="Пароль">
                        </div>
                        <div class="pb20 d_ib">
                          <a href="javascript:;" class="white_bd_lk">Забыли пароль?</a>
                        </div>
                        <div class="pb20 d_ib ml32">
                          <button class="red_btn">Войти</button>
                        </div>
                        <p class="bold">Или зарегистрируйтесь</p>
                        <div class="pb20  al_center">
                          <a href="javascript:;" class="white_bd_lk white_bd_l_1">Регистрация</a>
                        </div>
                          <p class="bold">Оформить без регистрации</p>
                        <div class="pb20">
                          <button class="red_btn red_bt_1">Быстрое оформление</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>