﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>News_inner</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
  <!--==============================header=================================-->
    <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
    <div id="content">
      <div class="sh_box">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-2 p_rel">
              <div class="catalog_box catalog_hidden">
                <h5 class="catalog_title">Каталог</h5>
                <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-lg-8">
              <ul class="breadcrumbs clearfix">
                <li class="breadcrumbs_item d_in-block">
                  <a href="sinergia_providers.html" class="breadcrumbs_link">
                    <span class="breadcrumbs_current">Поставщики</span>
                  </a>
                  /
                </li>
                <li class="breadcrumbs_item d_in-block">
                  <span class="breadcrumbs_current">ЗМ Products</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
              <div class="col-lg-2">
                <div class="news_data_general">
                  <strong>18</strong>
                  <span>10.14</span>
                </div>
              </div>
              <div class="col-lg-10">
                <h3 class="title1 mb10 mt-6">Россия выпускает свои материнские планы</h3>
                <p>
                  ЗАО «МЦСТ» объявило о начале опытного производства компактных материнских плат «Монокуб-М» на базе первых отечественных двухъядерных микропроцессоров «Эльбрус-2СМ».
                </p>
                <p>
                  Чипы «Эльбрус-2СМ» изготавливаются российской компанией «НИИМЭ и Микрон» с применением 90-нанометровой технологии. Эти процессоры, представляющие собой переработанный вариант «Эльбрус-2С », содержат два ядра и 2 Мбайт кеша. Используется два канала оперативной памяти DDR2-533. Отмечается, что отличительной особенностью «Эльбрус-2СМ» является его высокая производительность в задачах цифровой обработки сигналов и математических расчётах. Заявленное пиковое быстродействие чипа — более 12 млрд операций в секунду.
                </p>
                <p>
                  Что касается материнской платы «Монокуб-М», то она выполнена в форм-факторе mini-ITX. Среди имеющихся интерфейсов упомянуты PCI-Express 1.0, Gigabit Ethernet, USB 2.0, D-Sub, DVI, SATA 2.0, IDE (CompactFlash), RS-232, GPIO.
                </p>
                <p>
                  На основе «Монокуб-М», по заявлениям «МЦСТ», могут создаваться компактные компьютеры, моноблоки, мини-серверы, сетевые хранилища данных и пр. В комплекте с платой идёт операционная система «Эльбрус». О цене продукта не сообщается.
                </p>
              </div>
          </div>
        </div>
      </div>
    </div>
  <!--==============================footer=================================-->
    <?php include("main_blocks/footer.php") ?>
   
</body>
</html>