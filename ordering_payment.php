﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ordering payment</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body>
  
<!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
<!--==============================content================================-->
  <div id="content">
    <div class="sh_box1">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-2 p_rel">
            <div class="catalog_box catalog_hidden">
              <h5 class="catalog_title">Каталог</h5>
              <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
            <h2 class="title1 search_title">Оформление заказа</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="ordering_box">
      <div class="container-fluid">
          <ul class="ordering_delivery_list">
            <li class="ordering_list_item1"><a href="ordering_delivery.php" class="ordering_list_lk">Доставка</a></li>
            <li class="ordering_list_item2"><a href="ordering_payer.php" class="ordering_list_lk">Плательщик</a></li>
            <li class="ordering_list_item3"><a href="ordering_order.php" class="ordering_list_lk">Заказ</a></li>
            <li class="ordering_list_item4 current"><a href="ordering_payment.php" class="ordering_list_lk">Оплата</a></li>
          </ul>
      </div>
      <div class="bg_white">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
              <span class="d_block mb15">Выберите предпочитаемый способ оплаты</span>
              <div id="tabs" class="ordering_tabs clearfix pb20">
                <ul class="f_left ordering_tabs_list resp-tabs-list">
                  <li class="ordering_tabs_item">Оплата безналичным переводом<span class="d_block">1-3 рабочих дня</span></li>
                  <li class="ordering_tabs_item">Оплата картой Visa или Mastercard<span class="d_block">Мгновенно и без комиссий</span></li>
                  <li class="ordering_tabs_item">Квитанция в Сбербанк<span class="d_block">5-10 рабочих дней, комиссия около 3%</span></li>
                  <li class="ordering_tabs_item">Оплата через интерфейс вашего интернет-банка<span class="d_block">Без комиссий</span></li>
                  <li class="ordering_tabs_item">Оплата в салонах мобильной связи<span class="d_block">Может взиматься комиссия</span></li>
                  <li class="ordering_tabs_item">Оплата через платежные терминалы<span class="d_block">Может взиматься комиссия</span></li>
                </ul>
                <div class="f_left resp-tabs-container">
                  <div>
                      <span class="d_block">Безналичный перевод ваших средств в оплату за товар</span>
                  </div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                  <div>
                    <span class="d_block payments_message">Вы можете оплатить ваш заказ через платежные терминалы вашего города. Перевод осуществляется через безопасный платежный шлюз популярной системы RBK Money. Система поддерживает очень большой список платежных терминалов, перейдите к следующему шагу, чтобы увидеть полный список. Ваш перевод полностью безопасен, проводится через защищенный SSL протокол и информация о вашем заказе не передается сотрудникам платежной системы! Следуйте инструкциям платежной системы на следующей странице.</span>
                    <div class="payments_links">
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk1.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>ЛИДЕР</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk2.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>Элекснет</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk3.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>ФГПУ "Почта <br>России"</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk4.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>Свободная<br>касса</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk5.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>Промсвязьбанк</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk6.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>X-Plat</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk7.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>Мульти-касса</figcaption>
                      </figure>
                      <figure class="d_ib al_center">
                        <a href="#" class="payments_lk">
                          <img src="images/payments_lk8.jpg" height="53" width="87" alt="picture">
                        </a>
                        <figcaption>Telepay</figcaption>
                      </figure>
                    </div>
                    <div class="box_order_links pb20">
                      <button class="red_btn">Перейти к оплате</button>
                      <a href="#" class="black_lk">Назад</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--==============================footer=================================-->
 <?php include("main_blocks/footer.php") ?>
   
</body>
</html>