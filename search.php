﻿<!DOCTYPE html>
<html lang="en">
<head>
  <title>Search</title>
  <meta name = "format-detection" content = "telephone=no" />
  <meta charset="utf-8">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.1.1.js"></script>

  <!--[if lt IE 8]>
  <div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
      <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
    </a>
  </div>
  <![endif]-->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="js/html5.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
  <![endif]-->
</head>
<body class="">
  
  <!--==============================header=================================-->
  <?php include("main_blocks/header.php") ?>
  <!--==============================content================================-->
  <div id="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-2 p_rel">
          <div class="catalog_box catalog_hidden">
            <h5 class="catalog_title">Каталог</h5>
            <ul class="catalog_list">
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Интегральные микросхемы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Батареи и аккумуляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Аудио</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Предохранители, фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Кристаллы и осцилляторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Фильтры</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Карты и модули памяти</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Линзы, лупы, микроскопы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Потенциометры, переменные резисторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Реле</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Радиочастотные компоненты</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Трансформаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Конденсаторы</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Датчики, сенсоры</a>
                    <ul class="sub_catalog_list">
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Аксессуары</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Электролит-алюминиевые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Модульные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Пленочные</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Танталовые</a>
                      </li>
                      <li class="sub_catalog_item">
                        <a href="#" class="sub_catalog_link">Тонкопленочные</a>
                      </li>
                    </ul>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Дискретные полупроводники</a>
                  </li>
                  <li class="catalog_item">
                    <a href="#" class="catalog_link">Разъемы, соединители</a>
                  </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
        <h2 class="title1 search_title">Результаты поиска по запросу: <span class="counter_search">&laquo;232&raquo;, </span>найдено <span class="">32768</span> совпадений:</h2>
        </div>
      </div>
    </div>
    <div class="bg_black search_sorting_box">
      <div class="container-fluid">
        <div class="row">
          	<div class="col-lg-3">
          		<h4 class="title3 pb5">Производители</h4>
            		<div class="scroll-pane">
              <ul class="select_mades">
                <li class="form_box_check">
                  <input type="checkbox" id="made" class="d_ib all" checked>
                  <label class="provid_box_lb" for="made">Все</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made1" class="d_ib">
                  <label class="provid_box_lb" for="made1">Hammond Manufacturing</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made2" class="d_ib">
                  <label class="provid_box_lb" for="made2">Omron Automation and Safety</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made3" class="d_ib">
                  <label class="provid_box_lb" for="made3">Keystone Electronics</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made4" class="d_ib">
                  <label class="provid_box_lb" for="made4">Linx Technologies Inc</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made5" class="d_ib">
                  <label class="provid_box_lb" for="made5">Molex Inc</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made6" class="d_ib">
                  <label class="provid_box_lb" for="made6">Keystone Electronics</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made7" class="d_ib">
                  <label class="provid_box_lb" for="made7">Linx Technologies Inc</label>
                </li>
                <li class="form_box_check">
                  <input type="checkbox" id="made8" class="d_ib">
                  <label class="provid_box_lb" for="made8">Molex Inc</label>
                </li>
              </ul>
            		</div>
          	</div>
          	<div class="col-lg-3">
          		<h4 class="title3 pb5">Поставщики</h4>
              <div class="select_mades">
          		  <div class="form_box d_ib">
            	   <input type="checkbox" id="provid" class="d_ib all" checked>
            	   <label class="provid_box_lb" for="provid">Все</label>
          		  </div>
          		  <div class="form_box d_ib">
            	     <input type="checkbox" id="provid1" class="d_ib">
            	     <label class="provid_box_lb" for="provid1">Рога и копыта</label>
          		  </div>
          		  <div class="form_box d_ib">
            	     <input type="checkbox" id="provid2" class="d_ib">
            	     <label class="provid_box_lb" for="provid2">muRata</label>
          		  </div>
          		  <div class="form_box d_ib">
            	     <input type="checkbox" id="provid3" class="d_ib">
            	     <label class="provid_box_lb" for="provid3">Fairchild</label>
          		  </div>
          		  <div class="form_box d_ib">
            	     <input type="checkbox" id="provid4" class="d_ib">
            	     <label class="provid_box_lb" for="provid4">Farnell</label>
          		  </div>
              </div>
          	</div>
        	<div class="col-lg-3">
          		<div class="form_box pb18">
            		<h4 class="title3 pb5">Наличие у поставщика</h4>
            		<div class="w26 d_ib">
              			<input type="text" class="d_ib">
            		</div>
            		<div class="w15 d_ib">
              			<span class="braun d_ib units_inner">шт.</span>
            		</div>
          		</div>
          		<div class="form_box pb20">
            		<h4 class="title3 pb5">Цена за единицу</h4>
            		<input type="text" class="range_decimal" name="range_name" value="" />
          		</div>
        	</div>
        	<div class="col-lg-3 p_rel">
          		<div class="form_box pb26">
           			<h4 class="title3 pb5">Срок поставки</h4>
            		<input type="text" class="range_datetime" name="range_name" value="" />
          		</div>
          		<div class="form_box pb20 ">
            		<h4 class="title3 pb5">Минимальный заказ</h4>
            		<input type="text" class="range" name="range_name" value="" />
          		</div>
          		<button class="sorting_btn2 p_abs">Сброс</button>
        	</div>
      	</div>
      </div>
    </div>
    <div class="bg_white">
      <table class="order_tb mt20">
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <th class="col3 tab_aft_dwn">Наименование</th>
                  <th class="col3">Производитель</th>
                  <th class="col5">Описание</th>
                  <th class="col3">Склад</th>
                  <th class="col3">Цена</th>
                  <th class="col3">Поставщик</th>
                  <th class="col3">Артикул</th>
                  <th class="col4">Срок поставки</th>
                  <th class="col4"></th>
                </tr>
                <tr>
                  <td class="bold">MAX232ACPE+</td>
                  <td>Maxim Integrated</td>
                  <td>
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td>Maxim Integrated</td>
                  <td>A1303-ND</td>
                  <td>
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td>  
                  <td>
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none"> <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">ADM232AANZ</td>
                  <td class="col3">Texas Instruments</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Texas Instruments</td>
                  <td  class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td>
                  <td class="col4">
                    <div class="update_box al_center">
                      <input type="text">
                      <button class="red_btn">Обновить</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">ADM232AARNZ</td>
                  <td class="col3">Analog device</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                 </td>
                  <td class="col3">Analog device</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">MAX232ACPE+</td>
                  <td class="col3">Linear Technology</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Linear Technology</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">ADM232AANZ</td>
                  <td class="col3">Texas Instruments</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Texas Instruments</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">ADM232AARNZ</td>
                  <td class="col3">Analog device</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Analog device</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">MAX232ACPE+</td>
                  <td class="col3">Maxim Integrated</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Maxim Integrated</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">ADM232AANZ</td>
                  <td class="col3">Texas Instruments</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3"> 
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Texas Instruments</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="p_rel order_overlay">
            <div class="container-fluid">
              <table class="order_tb mt20">
                <tr>
                  <td class="bold col3">ADM232AARNZ</td>
                  <td class="col3">Analog device</td>
                  <td class="col5">
                    <span class="d_block">Standard Circular Connector</span>
                    <span class="d_block">RECEPTACLE 9 PIN shell size 13</span>
                    <a href="javascript:;" class="d_block pdf_btn">Datasheet</a>
                  </td>
                  <td class="col3"> 
                    <span class="d_block">3968 шт.</span>
                    <span class="d_block">Мин. заказ:1шт.</span>
                    <span class="d_block">Кратность:1шт.</span>
                  </td>
                  <td class="col3">
                    <div> 
                      <span class="d_ib product_num al_right">1шт.:</span><span class="d_ib"> 320,60 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">10шт.:</span><span class="d_ib"> 318,14 <span class="rub">a</span></span>
                    </div>
                    <div> 
                      <span class="d_ib product_num al_righ">50шт.:</span><span class="d_ib"> 306,78 <span class="rub">a</span></span>
                    </div>
                  </td>
                  <td class="col3">Analog device</td>
                  <td class="col3">A1303-ND</td>
                  <td class="col4">
                    <span class="delivery_time">4 недели</span>
                    <time datetime="2015-02-23" class="d_block"> 03.02.15</time>
                  </td> 
                  <td class="col4">
                    <div class="in_basket al_center">
                      <input type="text">
                      <button class="red_btn">В корзину</button>
                      <span class="add_btn d_none">Добавлено <span class="counter_added">30шт</span></span>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
      </table>
      <div class="al_center pb20">
        <a href="javascript:;" class="white_bd_lk">Показать еще</a>
      </div>
    </div>
  </div>
 
  <!--==============================footer=================================-->
  <?php include("main_blocks/footer.php") ?>
   
</body>
</html>