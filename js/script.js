$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
    var regM = /ipod|ipad|iphone/gi,
     result = ua.match(regM)
    if(!result) {
     $('.sf-menu li').each(function(){
      if($(">ul", this)[0]){
       $(">a", this).toggle(
        function(){
         return false;
        },
        function(){
         window.location.href = $(this).attr("href");
        }
       );
      } 
     })
    }
   } 
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')


// select
$.fn.extend({

    /**
    ** Emulates select form element
    ** @return jQuery
    **/
    customSelect : function(){
     var template = "<div class='active_option open_select'></div><ul class='options_list dropdown'></ul>";

     return this.each(function(){

      var $this = $(this);
      $this.prepend(template);

      var active = $this.children('.active_option'),
       list = $this.children('.options_list'),
       select = $this.children('select').hide(),
       options = select.children('option');


      active.text( 
       select.children('option[selected]').val() ? 
        select.children('option[selected]').val() : 
        options.eq(0).text()
      );

      options.each(function(){

       var optionOuter = $('<li></li>'),
        optionInner = $('<a></a>',{
         text : $(this).text(),
         href : '#',
         'data-value' : $(this).val()
        }),
        tpl = optionOuter.append(optionInner);


       list.append(tpl);
       
      });

      list.on("click", "a", function(event){

       event.preventDefault();

       var t = $(this).text(),
          v = $(this).attr('data-value');
       active.text(t);
       select.val(v);

       $(this).closest('.dropdown').removeClass("active");

      });

      $(document).click(function(event) {
        if ($(event.target).closest(".open_select").length) return;
        $(".custom_select").removeClass("active");
        event.stopPropagation();
      });

     });

    }});

$('.custom_select').customSelect();

$('.select_mades').on('change', 'input[type="checkbox"]', function(){
  var $this = $(this),
      collection = $this.closest('.select_mades').find('input').not('.all'),
      all = $this.closest('.select_mades').find('input.all');

  if($this.is(':checked') && $this.hasClass('all')){

    collection.each(function(i, el){
      el.checked = false;
    });
    
  }
  else if($this.is(':checked') && !$this.hasClass('all')){
    all[0].checked = false;
  }

});


$(document).ready(function(){

  // main slider background
  function mainSliderBackground(){
    var mainSliderWidth = $('#main_slider').width(),
        offset = $('.container-fluid').offset().left;
    $('.main_slider_bg').width(mainSliderWidth+offset+46);
  }
  if($(".main_slider_bg").length){
    mainSliderBackground();
    $(window).on('resize',mainSliderBackground);  
  }

  // main slider
  if($("#main_slider").length){
    $("#main_slider").owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:1
          }
      }
    });
  }
  // news slider
  if($(".news_slider").length){
    $(".news_slider").owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      responsive:{
          0:{
              items:4
        }
      }
    })
  }

  // providers slider
  if($(".providers_slider").length){
    $(".providers_slider").owlCarousel({
      loop:true,
      margin:45,
      nav:true,
      responsive:{
        0:{
            items:5
        }
      }
    })
  }
  
  $(".open_select,.animated_item").on('click',function(){
    $(this).parents(".custom_select").toggleClass('active');
  });

  // open select

  
  // $('.select_city .dropdown').each(function(){
  //   $(this).addClass('animated bounceOut')
  // });
  // $('.select_city .open_select,.select_city .animated_item').on('click',function(){
  //   var $this = $(this),
  //       custom_select = $this.parents('.custom_select');
  //   if(custom_select.hasClass('active')){
  //     $(custom_select).find('.dropdown').addClass('bounceOut');  
  //     $(custom_select).find('.dropdown').removeClass('bounceIn');
  //     $(custom_select).removeClass('active');
  //   }
  //   else{
  //     $(custom_select).find('.dropdown').addClass('bounceIn');  
  //     $(custom_select).find('.dropdown').removeClass('bounceOut');
  //     $(custom_select).addClass('active');
  //   }
    
  // });

  // dropdown
  $('.drop_down_btn').on('click',function(){
    $(this).parent().toggleClass('active');
  });
  
 $(document).click(function(event) {
        if ($(event.target).closest(".drop_down_box").length) return;
        $(".drop_down_box").removeClass("active");
      event.stopPropagation();
      });
  //
  $('.basket_closed').on('click',function(){
    $(this).parents("tr").remove("tr");
  });
  // inner table
  $('.orders_more_btn').click(function(){
        $(this)
          .closest("tr")
          .next('.orders_row_more')
          .find('.orders_more_box')
          .stop()
          .slideToggle()
          .parents('.order_overlay')
          .parent()
          .siblings()
          .find('.orders_more_box')
          .slideUp();
    });
  // click catalog
  $('.catalog_hidden').on('click',function(){
    $(this).find('.catalog_list').slideToggle().toggleClass('active');
    $(this).toggleClass('active');
  });

  //-------ion.rangeSlider----
  if($(".range").length){
    $(".range").ionRangeSlider({
      min: 0.00,
      max: 500.00,
      from:0,
      to: 10000,
      type: 'double',

    }); 
  }

  if($(".range_decimal").length){
    $(".range_decimal").ionRangeSlider({
      min: 0.10,
      max: 10000.00,
      from:0,
      step: .01,
      to: 10000,
      type: 'double',
      prettify_enabled: true,
      prettify: function (num) {
        return num.toFixed(2);
      }
    }); 
  }


   
  if($(".range_datetime").length){
    $('.range_datetime').ionRangeSlider({
      type: 'double',
      min: +moment().subtract(11, "months").format("X"),
      max: +moment().format("X"),
      from: +moment().subtract(6, "months").format("X"),
      prettify: function (num) {

          var date = moment(num, "X").format("LL"),
              dateString = date.split(" "),
              month,day, finalDate;

          switch(dateString[0]){

            case "January" :
              month = "01";
            break;

            case "February" :
              month = "02";
            break;

            case "March" :
              month = "03";
            break;

            case "April" :
              month = "04";
            break;

            case "May" :
              month = "05";
            break;

            case "June" :
              month = "06";
            break;

            case "July" :
              month = "07";
            break;

            case "August" :
              month = "08";
            break;

            case "September" :
              month = "09";
            break;

            case "October" :
              month = "10";
            break;

            case "November" :
              month = "11";
            break;

            case "December" :
              month = "12";
            break;

          }

          finalDate = (dateString[1].slice(0, -1).length == 1 ? "0" + dateString[1].slice(0, -1) : dateString[1].slice(0, -1)) + "." + month;

          return finalDate;
      }
    });
  }
  
  $(function($){
    if( $("#date").length || $(".phone").length){
      $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
      $(".phone").mask("+7(999) 999-99-99");
    }
  });
  //----scroll-pane

  if($(".scroll-pane").length){
	 $(".scroll-pane").mCustomScrollbar();
	}
  //-----responsive tabs----
  if($("#tabs").length){
    $( "#tabs" ).easyResponsiveTabs(); 
  }
   // ---------------удаление  объекта
  if ($(".delete_btn").length){
      $('.delete_btn').on('click',function(){
        $(this).parents('tr').remove()
      });
  }    
  //----skillbar---
  if($(".skillbar").length){
    $('.skillbar').each(function(){
      jQuery(this).find('.skillbar-bar').animate({
        width:jQuery(this).attr('data-percent')
      },1000);
    });
  }
  //----calendar---

  if($("#mydate").length){
    $('#mydate').glDatePicker({
       showAlways: true
    });
  }

});